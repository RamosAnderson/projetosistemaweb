﻿using Ftec.com.Sistema.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Windows.Forms;

namespace Ftec.com.Sistema.Controllers
{
    public class TipoAcaoController : Controller
    {
        // GET: TipoAcao
        public ActionResult Index()
        {
			List<TipoAcao> tipos = getListaTipoAcoes();
            return View(tipos);
        }

		public ActionResult Novo()
		{
			return View();
		}

		public ActionResult ProcessarGravacaoPost(TipoAcao tpAcao)
		{

			var tipos = getListaTipoAcoes();
			tipos.Add(tpAcao);

			Session["tpAcoes"] = tipos;


			return RedirectToAction("Index");
		}

		private List<TipoAcao> getListaTipoAcoes()
		{
			List<TipoAcao> tpAcoes;
			if (Session["tpAcoes"] == null)
			{
				tpAcoes = new List<TipoAcao>();
			}
			else
			{
				tpAcoes = (List<TipoAcao>)Session["tpAcoes"];
			}

			return tpAcoes;
		}

	}
}