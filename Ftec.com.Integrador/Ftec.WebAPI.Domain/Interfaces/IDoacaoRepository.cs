﻿using Ftec.WebAPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Domain.Interfaces
{
	public interface IDoacaoRepository
	{
		Guid Insert(Doacao doacao);
		Doacao Find(Guid id);
		List<Doacao> FindAll();
		Guid Update(Doacao doacao);
		bool Delete(Guid id);
	}
}
