﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ftec.com.Sistema.Models
{
	public class TipoDoacao
	{
		public Guid Id { get; set; }
		public string Descricao { get; set; }

		public TipoDoacao()
		{
			Id = Guid.NewGuid();
		}
	}
}