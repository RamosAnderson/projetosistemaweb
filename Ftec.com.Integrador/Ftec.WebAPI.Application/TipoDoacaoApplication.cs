﻿using Ftec.WebAPI.Application.Adapter;
using Ftec.WebAPI.Application.DTO;
using Ftec.WebAPI.Domain.Entities;
using Ftec.WebAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Application
{
	public class TipoDoacaoApplication
	{
		ITipoDoacaoRepository tipoDoacaoRepository;

		public TipoDoacaoApplication(ITipoDoacaoRepository tipoDoacaoRepository)
		{
			this.tipoDoacaoRepository = tipoDoacaoRepository;
		}

		public Guid Insert(TipoDoacaoDTO tpDoacaoDTO)
		{
			var tpDoacao = TipoDoacaoAdapter.ToDomain(tpDoacaoDTO);
			return tipoDoacaoRepository.Insert(tpDoacao);
		}

		public Guid Update(TipoDoacaoDTO tpDoacaoDTO)
		{
			var tpDoacao = TipoDoacaoAdapter.ToDomain(tpDoacaoDTO);
			return tipoDoacaoRepository.Update(tpDoacao);
		}

		public bool Delete(Guid id)
		{
			return tipoDoacaoRepository.Delete(id);
		}

		public TipoDoacaoDTO Get(Guid id)
		{
			var tpDoacao = tipoDoacaoRepository.Find(id);
			return TipoDoacaoAdapter.ToDTO(tpDoacao);
		}

		public List<TipoDoacaoDTO> GetAll()
		{
			List<TipoDoacaoDTO> tpDoacoesDto = new List<TipoDoacaoDTO>();
			var tpDoacoes = tipoDoacaoRepository.FindAll();

			foreach (TipoDoacao tpDpc in tpDoacoes)
			{
				tpDoacoesDto.Add(TipoDoacaoAdapter.ToDTO(tpDpc));
			}

			return tpDoacoesDto;
		}
	}
}
