﻿using Ftec.WebAPI.Domain.Entities;
using Ftec.WebAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Infra.Repository
{
	public class UsuarioRepository : IUsuarioRepository
	{
		private string connectionString;

		public UsuarioRepository (string connectionString)
		{
			this.connectionString = connectionString;
		}

		public bool Delete(Guid id)
		{
			throw new NotImplementedException();
		}

		public Usuario Find(Guid id)
		{
			throw new NotImplementedException();
		}

		public List<Usuario> FindAll()
		{
			throw new NotImplementedException();
		}

		public Guid Insert(Usuario usuario)
		{
			throw new NotImplementedException();
		}

		public Guid Update(Usuario usuario)
		{
			throw new NotImplementedException();
		}
	}
}
