﻿using Ftec.WebAPI.Application.DTO;
using Ftec.WebAPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Application.Adapter
{
	class VoluntarioAdapter
	{
		public static VoluntarioDTO ToDTO(Voluntario voluntario)
		{
			var volunt = new VoluntarioDTO()
			{
				Id       = voluntario.Id,
				Nome     = voluntario.Nome,
				Email    = voluntario.Email,
				Telefone = voluntario.Telefone,
				Cpf      = voluntario.Cpf,
				Senha    = voluntario.Senha,
				Ativo    = false,
				Endereco = new EnderecoDTO()
				{
					Id          = voluntario.Endereco.Id,
					Rua         = voluntario.Endereco.Rua,
					Cep         = voluntario.Endereco.Cep,
					Complemento = voluntario.Endereco.Complemento,
					Bairro      = voluntario.Endereco.Bairro,
					Numero      = voluntario.Endereco.Numero,
					Cidade      = voluntario.Endereco.Cidade
				}
			};

			foreach (TipoAcao afinidade in voluntario.Afinidade)
			{
				var tpAfim = new TipoAcaoDTO()
				{
					Id = afinidade.Id,
					Codigo = afinidade.Codigo,
					Descricao = afinidade.Descricao
				};

				volunt.Afinidade.Add(tpAfim);
			}

			return volunt;					
		}

		public static Voluntario ToDomain(VoluntarioDTO voluntario)
		{
			var volunt = new Voluntario(){
				Id       = voluntario.Id,
				Nome     = voluntario.Nome,
				Email    = voluntario.Email,
				Telefone = voluntario.Telefone,
				Cpf      = voluntario.Cpf,
				Senha    = voluntario.Senha,
				Ativo    = false,
				Endereco = new Endereco()
				{
					Id          = voluntario.Endereco.Id,
					Rua         = voluntario.Endereco.Rua,
					Cep         = voluntario.Endereco.Cep,
					Complemento = voluntario.Endereco.Complemento,
					Bairro      = voluntario.Endereco.Bairro,
					Numero      = voluntario.Endereco.Numero,
					Cidade      = voluntario.Endereco.Cidade
				}
			};

			foreach (TipoAcaoDTO afinidade in voluntario.Afinidade)
			{
				var tpAfinidade = new TipoAcao(){
					Id = afinidade.Id,
					Codigo = afinidade.Codigo,
					Descricao = afinidade.Descricao
				};

				volunt.Afinidade.Add(tpAfinidade);
			}

			return volunt;
		}
	}

}
