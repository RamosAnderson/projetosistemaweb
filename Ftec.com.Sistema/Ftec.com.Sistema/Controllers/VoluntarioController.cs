﻿using Ftec.com.Sistema.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ftec.com.Sistema.Controllers
{
    public class VoluntarioController : Controller
    {
        // GET: Voluntario
        public ActionResult Index()
        {
            return View();
        }

		public ActionResult Novo()
		{
			return View();
		}

		public ActionResult Editar(Guid Id)
		{
			if (Session["VoluntarioLogado"] != null)
			{
				Voluntario voluntario = (Voluntario)Session["VoluntarioLogado"];

				if (voluntario.Id == Id)
				{
					ViewBag.Voluntario = voluntario;
				}
			}

			return View();
		}

		public ActionResult Senha(Guid Id)
		{
			if (Session["VoluntarioLogado"] != null)
			{
				Voluntario voluntario = (Voluntario)Session["VoluntarioLogado"];

				if (voluntario.Id == Id)
				{
					ViewBag.Voluntario = voluntario;
				}
			}

			return View();
		}

		public ActionResult ProcessarGravacaoPost(Voluntario voluntario)
		{
			if (ModelState.IsValid)
			{												
				//Dispara e-mail para o admin
				//EnviaEmailAdminNovo();

				//Retorna para o Index com uma mensagem dizendo sucesso!
				return RedirectToAction("Index");				
			} else
			{
				return View("Novo", voluntario);
			}			
		}

		public ActionResult ProcessarAlteracaoPost(Voluntario voluntario)
		{
			Session["VoluntarioLogado"] = voluntario;
			return RedirectToAction ("Index");
		}

		public ActionResult ProcessarAlteraSenhaPost(Voluntario voluntario)
		{
			Voluntario voluntarioOld = (Voluntario)Session["VoluntarioLogado"];

			if (voluntarioOld.Senha == voluntario.Senha)
			{
				return RedirectToAction("Senha", voluntario);
			}

			if (voluntario.Senha == null || voluntario.Senha == "")
			{
				return RedirectToAction("Senha", voluntario);
			}

			Session["VoluntarioLogado"] = voluntario;
			return View("Index");
		}

		private void EnviaEmailAdminNovo()
		{

		}
	}
}