﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ftec.com.Administracao.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{			
			if (Session["UsuarioLogado"] == null)
			{
				return RedirectToAction("Login", "Usuario");
			}

			return View();
		}

		public ActionResult ChamarUsuarioPost()
		{
			return RedirectToAction("Index", "Usuario");
		}

		public ActionResult ChamarVoluntarioPost()
		{
			return RedirectToAction("Index", "Voluntario");
		}

		public ActionResult ChamarInstituicaoPost()
		{
			return RedirectToAction("Index", "Instituicao");
		}

		public ActionResult ChamarAcoesPost()
		{
			return RedirectToAction("Index", "Acao");
		}

		public ActionResult About()
		{
			ViewBag.Message = "Your application description page.";

			return View();
		}

		public ActionResult Contact()
		{
			ViewBag.Message = "Your contact page.";

			return View();
		}		
	}
}