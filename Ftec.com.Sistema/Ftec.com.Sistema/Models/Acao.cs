﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ftec.com.Sistema.Models
{
	public class Acao
	{
		public Guid Id { get; set; }
		public string Descricao { get; set; }
		public int Participantes { get; set; }
		public DateTime DataHora { get; set; }
		public Guid TipoAcao { get; set; }
		public Endereco Endereco { get; set; }
		
		
		public Acao()
		{
			Id = Guid.NewGuid();
		}
	}
}