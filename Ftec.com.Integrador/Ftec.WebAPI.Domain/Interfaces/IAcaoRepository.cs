﻿using Ftec.WebAPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Domain.Interfaces
{
	public interface IAcaoRepository
	{
		Guid Insert(Acao acao);
		Acao Find(Guid id);
		List<Acao> FindAll();
		Guid Update(Acao acao);
		bool Delete(Guid id);
	}
}
