﻿using Ftec.com.Administracao.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ftec.com.Administracao.Controllers
{
    public class InstituicaoController : Controller
    {
        // GET: Instituicao
        public ActionResult Index()
        {
			var instituicoes = GetListaInstituicao();
			if (!instituicoes.Any())
			{
				criarinstituicoesPendentes(instituicoes);
				Session["instituicoes"] = instituicoes;
			}

			return View(instituicoes);			
        }

		public ActionResult Visualizar(Guid Id)
		{
			var instituicoes = GetListaInstituicao();
			var instituicao = instituicoes.FirstOrDefault(l => l.Id == Id);

			ViewBag.Instituicao = instituicao;

			return View();
		}

		public ActionResult Ativar(Guid Id)
		{
			var instituicoes =  GetListaInstituicao();
			var instituicao = instituicoes.FirstOrDefault(l => l.Id == Id);

			instituicao.Ativo = true;
			instituicoes.Remove(instituicao);

			return RedirectToAction("Index");
		}

		private List<Instituicao> GetListaInstituicao()
		{
			List<Instituicao> instituicoes;

			if (Session["instituicoes"] == null)
			{
				instituicoes = new List<Instituicao>();
			}
			else
			{
				instituicoes = (List<Instituicao>)Session["instituicoes"];
			}

			return instituicoes;
		}
		private void criarinstituicoesPendentes(List<Instituicao> instituicoes)
		{
			var instiuicao1 = new Instituicao();
			instiuicao1.Razao = "UniFtec";
			instiuicao1.Email = "Uniftec@uniftec.com";
			instiuicao1.Ativo = false;
			instituicoes.Add(instiuicao1);

			var instiuicao2 = new Instituicao();
			instiuicao2.Razao = "Ucs";
			instiuicao2.Email = "Ucs@ucs.com.br";
			instiuicao2.Ativo = false;
			instituicoes.Add(instiuicao2);
		}

	}
}