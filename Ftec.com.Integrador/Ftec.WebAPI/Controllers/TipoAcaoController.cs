﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Ftec.WebAPI.Application;
using Ftec.WebAPI.Application.DTO;
using Ftec.WebAPI.Domain.Interfaces;
using Ftec.WebAPI.Infra.Repository;
using Ftec.WebAPI.Models;


namespace Ftec.WebAPI.Controllers
{
	public class TipoAcaoController : ApiController
	{
		private ITipoAcaoRepository tipoAcaoRepository;
		private TipoAcaoApplication tipoAcaoApplication;

		public TipoAcaoController()
		{
			string connectionString = string.Empty;

			tipoAcaoRepository  = new Ftec.WebAPI.Infra.PsqlRepository.TipoAcaoRepository(connectionString);
			tipoAcaoApplication = new TipoAcaoApplication(tipoAcaoRepository);
		}

		public HttpResponseMessage Get()
		{
			try
			{
				List<TipoAcao> tpAcoes = ProcurarTodos();
				return Request.CreateResponse(HttpStatusCode.OK, tpAcoes);
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		public HttpResponseMessage Get(Guid id)
		{
			try
			{
				TipoAcao tpAcao = Procurar(id);

				if (tpAcao == null)
				{
					return Request.CreateResponse(HttpStatusCode.NotFound, "Registro não encontrado!");
				}
				else
				{
					return Request.CreateResponse(HttpStatusCode.OK, tpAcao);
				}
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		public HttpResponseMessage Post([FromBody]TipoAcao tpAcao)
		{
			try
			{
				Guid id = Inserir(tpAcao);
				return Request.CreateResponse(HttpStatusCode.OK, id);
			}
			catch (ApplicationException apEx)
			{
				return Request.CreateErrorResponse(HttpStatusCode.BadRequest, apEx.Message);
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		public HttpResponseMessage Put(Guid id, [FromBody]TipoAcao tpAcao)
		{
			try
			{
				TipoAcao tpAction = Procurar(id);
				if (tpAction == null)
				{
					return Request.CreateResponse(HttpStatusCode.NotFound, "Tipo de acao não encontrado!");
				}
				else
				{
					Alterar(tpAcao);
					return Request.CreateResponse(HttpStatusCode.OK, id);
				}
			}
			catch (ApplicationException apEx)
			{
				return Request.CreateErrorResponse(HttpStatusCode.BadRequest, apEx.Message);
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		public HttpResponseMessage Delete(Guid id)
		{
			try
			{
				TipoAcao tpAcao = Procurar(id);
				if (tpAcao == null)
				{
					return Request.CreateResponse(HttpStatusCode.NotFound, "TIpo de Acao não encontrado");
				}
				else
				{
					Excluir(id);
					return Request.CreateResponse(HttpStatusCode.OK, id);
				}
			}
			catch (ApplicationException apEx)
			{
				return Request.CreateErrorResponse(HttpStatusCode.BadRequest, apEx.Message);
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}


		/*METODOS PRIVADOS DO CONTROLLER*/

		private List<TipoAcao> ProcurarTodos()
		{
			var tpAcoes = new List<TipoAcao>();
			var tpAcoesDTO = tipoAcaoApplication.GetAll();

			foreach (TipoAcaoDTO tpAcaoDTO in tpAcoesDTO)
			{
				tpAcoes.Add(new TipoAcao()
				{
					Id = tpAcaoDTO.Id,
					Codigo = tpAcaoDTO.Codigo,
					Descricao = tpAcaoDTO.Descricao
				});
			}

			return tpAcoes;
		}

		private TipoAcao Procurar(Guid id)
		{
			var tipoAcaoDTO = tipoAcaoApplication.Get(id);

			return new TipoAcao()
			{
				Id        = tipoAcaoDTO.Id,
				Codigo    = tipoAcaoDTO.Codigo,
				Descricao = tipoAcaoDTO.Descricao
			};
		}

		private Guid Inserir(TipoAcao tpAcao)
		{
			TipoAcaoDTO tpAcaoDTO = new TipoAcaoDTO()
			{
				Id = tpAcao.Id,
				Codigo = tpAcao.Codigo,
				Descricao = tpAcao.Descricao
			};

			return tipoAcaoApplication.Insert(tpAcaoDTO);
		}

		private void Alterar(TipoAcao tpAcao)
		{
			TipoAcaoDTO tpAcaoDTO = new TipoAcaoDTO()
			{
				Id = tpAcao.Id,
				Codigo = tpAcao.Codigo,
				Descricao = tpAcao.Descricao
			};

			tipoAcaoApplication.Update(tpAcaoDTO);
		}

		private void Excluir(Guid id)
		{
			tipoAcaoApplication.Delete(id);
		}
	}
}