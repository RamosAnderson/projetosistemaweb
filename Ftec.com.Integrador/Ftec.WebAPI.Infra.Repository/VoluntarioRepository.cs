﻿using Ftec.WebAPI.Domain.Entities;
using Ftec.WebAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Infra.Repository 
{
	public class VoluntarioRepository : IVoluntarioRepository
	{
		private string connectionString;

		public VoluntarioRepository(string connectionString)
		{
			this.connectionString = connectionString;
		}

		public bool Delete(Guid id)
		{
			throw new NotImplementedException();
		}

		public Voluntario Find(Guid id)
		{
			throw new NotImplementedException();
		}

		public List<Voluntario> FindAll()
		{
			throw new NotImplementedException();
		}

		public Guid Insert(Voluntario voluntario)
		{
			throw new NotImplementedException();
		}

		public Guid Update(Voluntario voluntario)
		{
			throw new NotImplementedException();
		}
	}
}
