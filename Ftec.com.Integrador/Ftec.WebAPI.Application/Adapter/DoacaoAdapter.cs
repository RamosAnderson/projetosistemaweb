﻿using Ftec.WebAPI.Application.DTO;
using Ftec.WebAPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Application.Adapter
{
	public static class DoacaoAdapter
	{
		public static DoacaoDTO ToDTO(Doacao doacao)
		{
			return new DoacaoDTO()
			{
				Id = doacao.Id,
				Descricao = doacao.Descricao,
				Disponivel = doacao.Disponivel,
				Valor = doacao.Valor,
				Tipo = new TipoDoacaoDTO()
				{
					Id = doacao.Tipo.Id,
					Codigo = doacao.Tipo.Codigo,
					Descricao = doacao.Tipo.Descricao
				}
			};
		}

		public static Doacao ToDomain(DoacaoDTO doacao)
		{
			return new Doacao()
			{
				Id = doacao.Id,
				Descricao = doacao.Descricao,
				Disponivel = doacao.Disponivel,
				Valor = doacao.Valor,
				Tipo = new TipoDoacao()
				{
					Id = doacao.Tipo.Id,
					Codigo = doacao.Tipo.Codigo,
					Descricao = doacao.Tipo.Descricao
				}
			};
		}
	}

}
