﻿using Ftec.WebAPI.Domain.Entities;
using Ftec.WebAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Infra.Repository
{
	public class EnderecoRepository : IEnderecoRepository
	{
		private string connectionString;

		public EnderecoRepository(string connectionString)
		{
			this.connectionString = connectionString;
		}

		public bool Delete(Guid id)
		{
			throw new NotImplementedException();
		}

		public Endereco Find(Guid id)
		{
			throw new NotImplementedException();
		}

		public List<Endereco> FindAll()
		{
			throw new NotImplementedException();
		}

		public Guid Insert(Endereco endereco)
		{
			throw new NotImplementedException();
		}

		public Guid Update(Endereco endereco)
		{
			throw new NotImplementedException();
		}
	}
}
