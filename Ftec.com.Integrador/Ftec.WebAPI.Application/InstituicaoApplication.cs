﻿using Ftec.WebAPI.Application.Adapter;
using Ftec.WebAPI.Application.DTO;
using Ftec.WebAPI.Domain.Entities;
using Ftec.WebAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Application
{
	public class InstituicaoApplication
	{
		IInstituicaoRepository instituicaoRepository;

		public InstituicaoApplication(IInstituicaoRepository instituicaoRepository)
		{
			this.instituicaoRepository = instituicaoRepository;
		}

		public Guid Insert(InstituicaoDTO InstituicaoDTO)
		{
			var instituicao = InstituicaoAdapter.ToDomain(InstituicaoDTO);
			return instituicaoRepository.Insert(instituicao);
		}

		public Guid Update(InstituicaoDTO InstituicaoDTO)
		{
			var instituicao = InstituicaoAdapter.ToDomain(InstituicaoDTO);
			return instituicaoRepository.Update(instituicao);
		}

		public bool Delete(Guid id)
		{
			return instituicaoRepository.Delete(id);
		}

		public InstituicaoDTO Get(Guid id)
		{
			var instituicao = instituicaoRepository.Find(id);
			return InstituicaoAdapter.ToDTO(instituicao);
		}

		public List<InstituicaoDTO> GetAll()
		{
			List<InstituicaoDTO> instituicoesDto = new List<InstituicaoDTO>();
			var instituicoes = instituicaoRepository.FindAll();

			foreach (Instituicao inst in instituicoes)
			{
				instituicoesDto.Add(InstituicaoAdapter.ToDTO(inst));
			}

			return instituicoesDto;
		}
	}
}
