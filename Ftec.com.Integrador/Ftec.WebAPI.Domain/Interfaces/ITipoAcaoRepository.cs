﻿using Ftec.WebAPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Domain.Interfaces
{
	public interface ITipoAcaoRepository
	{
		Guid Insert(TipoAcao tpAcao);
		TipoAcao Find(Guid id);
		List<TipoAcao> FindAll();
		Guid Update(TipoAcao tpAcao);
		bool Delete(Guid id);
	}
}
