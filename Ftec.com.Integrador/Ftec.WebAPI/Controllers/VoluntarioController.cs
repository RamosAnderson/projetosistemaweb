﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Ftec.WebAPI.Application;
using Ftec.WebAPI.Application.DTO;
using Ftec.WebAPI.Domain.Interfaces;
using Ftec.WebAPI.Infra.Repository;
using Ftec.WebAPI.Models;


namespace Ftec.WebAPI.Controllers
{
	public class VoluntarioController : ApiController
	{
		private IVoluntarioRepository voluntarioRepository;
		private VoluntarioApplication voluntarioApplication;

		public VoluntarioController()
		{
			string connectionString = string.Empty;

			voluntarioRepository = new Ftec.WebAPI.Infra.PsqlRepository.VoluntarioRepository(connectionString);
			voluntarioApplication = new VoluntarioApplication(voluntarioRepository);
		}

		public HttpResponseMessage Get()
		{
			try
			{
				List<Voluntario> voluntarios = ProcurarTodos();
				return Request.CreateResponse(HttpStatusCode.OK, voluntarios);
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		public HttpResponseMessage Get(Guid id)
		{
			try
			{
				Voluntario voluntario = Procurar(id);
				if (voluntario == null)
				{
					return Request.CreateResponse(HttpStatusCode.NotFound, "Registro não encontrado!");
				}
				else
				{
					return Request.CreateResponse(HttpStatusCode.OK, voluntario);
				}
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		public HttpResponseMessage Post([FromBody]Voluntario voluntario)
		{
			try
			{
				Guid id = Inserir(voluntario);
				return Request.CreateResponse(HttpStatusCode.OK, id);
			}
			catch (ApplicationException ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		public HttpResponseMessage Put(Guid id, [FromBody]Voluntario voluntario)
		{
			try
			{
				Voluntario volunt = Procurar(id);
				if (volunt == null)
				{
					return Request.CreateResponse(HttpStatusCode.NotFound, "Voluntario a ser alterado não foi encontrado!");
				}
				else
				{
					Alterar(voluntario);
					return Request.CreateResponse(HttpStatusCode.OK, id);
				}
			}
			catch (ApplicationException ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		public HttpResponseMessage Delete(Guid id)
		{
			try
			{
				Voluntario voluntario = Procurar(id);
				if (voluntario == null)
				{
					return Request.CreateResponse(HttpStatusCode.NotFound, "Voluntario a ser excluido não foi encontrado!");
				}
				else
				{
					Excluir(id);
					return Request.CreateResponse(HttpStatusCode.OK, id);
				}

			}
			catch (ApplicationException ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		/*MÉTODOS PRIVADOS DO CONTROLLER*/
		private List<Voluntario> ProcurarTodos()
		{
			var voluntarios = new List<Voluntario>();
			var voluntariosDTO = voluntarioApplication.GetAll();

			foreach (VoluntarioDTO voluntarioDTO in voluntariosDTO)
			{
				var voluntAux = new Voluntario()
				{
					Id = voluntarioDTO.Id,
					Ativo = voluntarioDTO.Ativo,
					Cpf = voluntarioDTO.Cpf,
					Email = voluntarioDTO.Email,
					Nome = voluntarioDTO.Nome,
					Senha = voluntarioDTO.Senha,
					Telefone = voluntarioDTO.Telefone,
					Endereco = new Endereco()
					{
						Id = voluntarioDTO.Endereco.Id,
						Bairro = voluntarioDTO.Endereco.Bairro,
						Cep = voluntarioDTO.Endereco.Cep,
						Cidade = voluntarioDTO.Endereco.Cidade,
						Complemento = voluntarioDTO.Endereco.Complemento,
						Numero = voluntarioDTO.Endereco.Numero,
						Rua = voluntarioDTO.Endereco.Rua

					}
				};

				foreach (TipoAcaoDTO afinidade in voluntarioDTO.Afinidade)
				{
					var tpAfim = new TipoAcao()
					{
						Id = afinidade.Id,
						Codigo = afinidade.Codigo,
						Descricao = afinidade.Descricao
					};

					voluntAux.Afinidade.Add(tpAfim);
				}

				voluntarios.Add(voluntAux);
			}

			return voluntarios;
		}

		private Voluntario Procurar(Guid id)
		{
			var voluntario = voluntarioApplication.Get(id);

			var voluntAux = new Voluntario()
			{
				Id = voluntario.Id,
				Ativo = voluntario.Ativo,
				Cpf = voluntario.Cpf,
				Email = voluntario.Email,
				Nome = voluntario.Nome,
				Senha = voluntario.Senha,
				Telefone = voluntario.Telefone,
				Endereco = new Endereco()
				{
					Id = voluntario.Endereco.Id,
					Bairro = voluntario.Endereco.Bairro,
					Cep = voluntario.Endereco.Cep,
					Cidade = voluntario.Endereco.Cidade,
					Complemento = voluntario.Endereco.Complemento,
					Numero = voluntario.Endereco.Numero,
					Rua = voluntario.Endereco.Rua

				}
			};

			foreach (TipoAcaoDTO afinidade in voluntario.Afinidade)
			{
				var tpAfim = new TipoAcao()
				{
					Id = afinidade.Id,
					Codigo = afinidade.Codigo,
					Descricao = afinidade.Descricao
				};

				voluntAux.Afinidade.Add(tpAfim);
			}

			return voluntAux;
		}

		private Guid Inserir(Voluntario voluntario)
		{
			VoluntarioDTO voluntDTO = new VoluntarioDTO()
			{
				Id = voluntario.Id,
				Ativo = voluntario.Ativo,
				Cpf = voluntario.Cpf,
				Email = voluntario.Email,
				Nome = voluntario.Nome,
				Senha = voluntario.Senha,
				Telefone = voluntario.Telefone,
				Endereco = new EnderecoDTO()
				{
					Id = voluntario.Endereco.Id,
					Bairro = voluntario.Endereco.Bairro,
					Cep = voluntario.Endereco.Cep,
					Cidade = voluntario.Endereco.Cidade,
					Complemento = voluntario.Endereco.Complemento,
					Numero = voluntario.Endereco.Numero,
					Rua = voluntario.Endereco.Rua

				}
			};

			foreach (TipoAcao afinidade in voluntario.Afinidade)
			{
				var tpAfim = new TipoAcaoDTO()
				{
					Id = afinidade.Id,
					Codigo = afinidade.Codigo,
					Descricao = afinidade.Descricao
				};

				voluntDTO.Afinidade.Add(tpAfim);
			}

			return voluntarioApplication.Insert(voluntDTO);
		}

		private void Alterar(Voluntario voluntario)
		{
			VoluntarioDTO voluntDTO = new VoluntarioDTO()
			{
				Id = voluntario.Id,
				Ativo = voluntario.Ativo,
				Cpf = voluntario.Cpf,
				Email = voluntario.Email,
				Nome = voluntario.Nome,
				Senha = voluntario.Senha,
				Telefone = voluntario.Telefone,
				Endereco = new EnderecoDTO()
				{
					Id = voluntario.Endereco.Id,
					Bairro = voluntario.Endereco.Bairro,
					Cep = voluntario.Endereco.Cep,
					Cidade = voluntario.Endereco.Cidade,
					Complemento = voluntario.Endereco.Complemento,
					Numero = voluntario.Endereco.Numero,
					Rua = voluntario.Endereco.Rua

				}
			};

			foreach (TipoAcao afinidade in voluntario.Afinidade)
			{
				var tpAfim = new TipoAcaoDTO()
				{
					Id = afinidade.Id,
					Codigo = afinidade.Codigo,
					Descricao = afinidade.Descricao
				};

				voluntDTO.Afinidade.Add(tpAfim);
			}

			voluntarioApplication.Update(voluntDTO);
		}

		private void Excluir(Guid id)
		{
			voluntarioApplication.Delete(id);
		}
	}
}