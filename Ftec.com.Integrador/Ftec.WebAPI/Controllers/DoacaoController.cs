﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Ftec.WebAPI.Application;
using Ftec.WebAPI.Application.DTO;
using Ftec.WebAPI.Domain.Interfaces;
using Ftec.WebAPI.Infra.Repository;
using Ftec.WebAPI.Models;


namespace Ftec.WebAPI.Controllers
{
	public class DoacaoController : ApiController
	{
		private IDoacaoRepository doacaoRepository;
		private DoacaoApplication doacaoApplication;

		public DoacaoController()
		{
			string connectionString = string.Empty;

			doacaoRepository  = new Ftec.WebAPI.Infra.PsqlRepository.DoacaoRepository(connectionString);
			doacaoApplication = new DoacaoApplication(doacaoRepository);
		}

		public HttpResponseMessage Get()
		{
			try
			{
				List<Doacao> doacoes = ProcurarTodos();
				return Request.CreateResponse(HttpStatusCode.OK, doacoes);

			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		public HttpResponseMessage Get(Guid id)
		{
			try
			{
				Doacao doacao = Procurar(id);

				if (doacao == null)
				{
					return Request.CreateResponse(HttpStatusCode.NotFound, "Doacao não encontrada!");
				}
				else
				{
					return Request.CreateResponse(HttpStatusCode.OK, id);
				}
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		public HttpResponseMessage Post([FromBody] Doacao doacao)
		{
			try
			{
				Guid id = Inserir(doacao);
				return Request.CreateResponse(HttpStatusCode.OK, id);
			}
			catch (ApplicationException ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		public HttpResponseMessage Put(Guid id, [FromBody] Doacao doacao)
		{
			try
			{
				Doacao doacaoAux = Procurar(id);
				if (doacaoAux == null)
				{
					return Request.CreateResponse(HttpStatusCode.NotFound, "Doacao Não Encontrada!");
				}
				else
				{
					Alterar(doacao);
					return Request.CreateResponse(HttpStatusCode.OK, id);
				}
			}
			catch (ApplicationException ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		public HttpResponseMessage Delete(Guid id)
		{
			try
			{
				Doacao doacao = Procurar(id);
				if (doacao == null)
				{
					return Request.CreateResponse(HttpStatusCode.NotFound, "Doacao a ser excluida, não foi encontrada");
				}
				else
				{
					Excluir(id);
					return Request.CreateResponse(HttpStatusCode.OK, id);
				}
				
			}
			catch (ApplicationException ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		/*MÉTODOS PRIVADOS DO CONTROLLER*/
		private List<Doacao> ProcurarTodos()
		{
			var doacoes = new List<Doacao>();
			var doacoesDTO = doacaoApplication.GetAll();

			foreach(DoacaoDTO doacaoDTO in doacoesDTO)
			{
				doacoes.Add(new Doacao()
				{
					Id = doacaoDTO.Id,
					Descricao = doacaoDTO.Descricao,
					Disponivel = doacaoDTO.Disponivel,
					Valor = doacaoDTO.Valor,
					Tipo = new TipoDoacao()
					{
						Id = doacaoDTO.Tipo.Id,
						Codigo = doacaoDTO.Tipo.Codigo,
						Descricao = doacaoDTO.Tipo.Descricao
					}
				});
			}

			return doacoes;
		}

		private Doacao Procurar(Guid id)
		{
			var doacaoDTO = doacaoApplication.Get(id);

			return new Doacao()
			{
				Id = doacaoDTO.Id,
				Descricao = doacaoDTO.Descricao,
				Disponivel = doacaoDTO.Disponivel,
				Valor = doacaoDTO.Valor,
				Tipo = new TipoDoacao()
				{
					Id = doacaoDTO.Tipo.Id,
					Codigo = doacaoDTO.Tipo.Codigo,
					Descricao = doacaoDTO.Tipo.Descricao
				}
			};
		}

		private Guid Inserir(Doacao doacao)
		{
			DoacaoDTO doacaoDTO = new DoacaoDTO()
			{
				Id = doacao.Id,
				Descricao = doacao.Descricao,
				Disponivel = doacao.Disponivel,
				Valor = doacao.Valor,
				Tipo = new TipoDoacaoDTO()
				{
					Id = doacao.Tipo.Id,
					Codigo = doacao.Tipo.Codigo,
					Descricao = doacao.Tipo.Descricao
				}
			};

			return doacaoApplication.Insert(doacaoDTO);
		}

		private void Alterar(Doacao doacao)
		{
			DoacaoDTO doacaoDTO = new DoacaoDTO()
			{
				Id = doacao.Id,
				Descricao = doacao.Descricao,
				Disponivel = doacao.Disponivel,
				Valor = doacao.Valor,
				Tipo = new TipoDoacaoDTO()
				{
					Id = doacao.Tipo.Id,
					Codigo = doacao.Tipo.Codigo,
					Descricao = doacao.Tipo.Descricao
				}
			};

			doacaoApplication.Update(doacaoDTO);
		}

		private void Excluir(Guid id)
		{
			doacaoApplication.Delete(id);
		}

	}
}