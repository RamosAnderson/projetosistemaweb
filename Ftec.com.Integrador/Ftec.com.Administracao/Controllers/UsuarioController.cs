﻿using Ftec.com.Administracao.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ftec.com.Administracao.Controllers
{
    public class UsuarioController : Controller
    {
		private API.APIHttpClient usuarioHttp;
		public UsuarioController()
		{
			usuarioHttp = new API.APIHttpClient("http://localhost:51613/api/");

		}

        // GET: Usuario
        public ActionResult Index()
        {
			var usuarios = usuarioHttp.Get<List<Usuario>>(@"usuario");				
            return View(usuarios);
        }
		
		public ActionResult Novo()
		{
			return View();
		}

		public ActionResult Editar(Guid Id)
		{
			var usuario = usuarioHttp.Get<Usuario>(string.Format(@"usuario/{0}", Id.ToString()));
			ViewBag.Usuario = usuario;

			return View();
		}

		public ActionResult Excluir(Guid Id)
		{
			var usuario = usuarioHttp.Get<Usuario>(string.Format(@"usuario/{0}", Id.ToString()));
			
			//Session["users"] = usuarios;

			return RedirectToAction("Index");
		}

		public ActionResult Login()
		{
			return View();
		}	

		[HttpPost]
		public ActionResult ProcessarGravacaoPost(Usuario usuario)
		{
			if (ModelState.IsValid)
			{

				var id = usuarioHttp.Post<Usuario>(@"usuario/", usuario);
				
				return RedirectToAction("Index");
			}
			else
			{
				return View("Novo", usuario);
			}
		}

		public ActionResult ProcessarAlteracaoPost(Usuario usuario)
		{
			var id = usuarioHttp.Put<Usuario>(@"usuario/", usuario.Id, usuario);
						
			return RedirectToAction("Index");
		}

		public ActionResult ProcessarLoginPost(Usuario usuario)
		{
			var user = usuarioHttp.Get<Usuario>(string.Format(@"usuario/{0}", usuario.Id.ToString()));

			
			if (usuario.Email == user.Email &&
				usuario.Senha == user.Senha)
			{
				Session["UsuarioLogado"] = user.Id;
				return RedirectToAction("Index", "Home");
			}
			
			return View("Usuário não encontrado!");
		}

		public ActionResult ProcessarSaidaPost()
		{
			Session["UsuarioLogado"] = null;
			return RedirectToAction("Index", "Home");
		}

		public List<Usuario> GetListaUsers()
		{

			List<Usuario> usuarios;
			//
			if (Session["users"] == null)
			{
				usuarios = new List<Usuario>();
			}
			else
			{
				usuarios = (List<Usuario>)Session["users"];
			}

			return usuarios;
		}

	}
}