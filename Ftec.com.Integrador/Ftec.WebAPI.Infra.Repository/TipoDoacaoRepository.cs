﻿using Ftec.WebAPI.Domain.Entities;
using Ftec.WebAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Infra.Repository
{
	public class TipoDoacaoRepository : ITipoDoacaoRepository
	{

		private string connectionString;

		public TipoDoacaoRepository(string connectionString)
		{
			this.connectionString = connectionString;
		}
		public bool Delete(Guid id)
		{
			throw new NotImplementedException();
		}

		public Domain.Entities.TipoDoacao Find(Guid id)
		{
			throw new NotImplementedException();
		}

		public List<Domain.Entities.TipoDoacao> FindAll()
		{
			throw new NotImplementedException();
		}

		public Guid Insert(Domain.Entities.TipoDoacao tpDoacao)
		{
			throw new NotImplementedException();
		}

		public Guid Update(Domain.Entities.TipoDoacao tpDoacao)
		{
			throw new NotImplementedException();
		}
	}
}
