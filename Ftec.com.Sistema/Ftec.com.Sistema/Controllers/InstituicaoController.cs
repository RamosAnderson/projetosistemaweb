﻿using Ftec.com.Sistema.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ftec.com.Sistema.Controllers
{
    public class InstituicaoController : Controller
    {
        // GET: Instituicao
        public ActionResult Index()
        {
            return View();
        }

		public ActionResult Novo()
		{
			return View();
		}

		public ActionResult Editar(Guid Id)
		{
			if (Session["InstituicaoLogado"] != null)
			{
				Instituicao instituicao = (Instituicao)Session["InstituicaoLogado"];

				if (instituicao.Id == Id)
				{
					ViewBag.Instituicao = instituicao;
				}
			}

			return View();
		}

		public ActionResult Senha(Guid Id)
		{
			if (Session["InstituicaoLogado"] != null)
			{
				Instituicao instituicao = (Instituicao)Session["InstituicaoLogado"];

				if (instituicao.Id == Id)
				{
					ViewBag.Instituicao = instituicao;
				}
			}

			return View();
		}

		public ActionResult ProcessarGravacaoPost(Instituicao instituicao)
		{
			if (ModelState.IsValid)
			{
				//Dispara e-mail para o admin
				//EnviaEmailAdminNovo();

				//Retorna para o Index com uma mensagem dizendo sucesso!
				return RedirectToAction("Index");
			}
			else
			{
				return View("Novo", instituicao);
			}
		}

		public ActionResult ProcessarAlteracaoPost(Instituicao instituicao)
		{
			Session["InstituicaoLogado"] = instituicao;
			return RedirectToAction("Index");
		}

		public ActionResult ProcessarAlteraSenhaPost(Instituicao instituicao)
		{
			Instituicao instituicaoOld = (Instituicao)Session["InstituicaoLogado"];

			if (instituicaoOld.Senha == instituicao.Senha)
			{
				return RedirectToAction("Senha", instituicao);
			}

			if (instituicao.Senha == null || instituicao.Senha == "")
			{
				return RedirectToAction("Senha", instituicao);
			}

			Session["InstituicaoLogado"] = instituicao;
			return View("Index");
		}
	}
}