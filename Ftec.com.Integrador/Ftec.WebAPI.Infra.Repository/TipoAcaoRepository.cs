﻿using Ftec.WebAPI.Domain.Entities;
using Ftec.WebAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Infra.Repository
{
	public class TipoAcaoRepository : ITipoAcaoRepository
	{
		private string connectionString;

		public TipoAcaoRepository(string connectionString)
		{
			this.connectionString = connectionString;
		}

		public bool Delete(Guid id)
		{
			throw new NotImplementedException();
		}

		public TipoAcao Find(Guid id)
		{
			throw new NotImplementedException();
		}

		public List<TipoAcao> FindAll()
		{
			throw new NotImplementedException();
		}

		public Guid Insert(TipoAcao tpAcao)
		{
			throw new NotImplementedException();
		}

		public Guid Update(TipoAcao tpAcao)
		{
			throw new NotImplementedException();
		}
	}
}
