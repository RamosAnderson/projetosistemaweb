﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Ftec.com.Administracao.Models
{
	public class Usuario
	{
		public Guid   Id    { get; set; }
		[Required(ErrorMessage = "Nome é obrigatorio")]
		public string Nome  { get; set; }
		public string Email { get; set; }
		public string Senha { get; set; }

		public Usuario()
		{
			Id = Guid.NewGuid();
		}
	}

	
}