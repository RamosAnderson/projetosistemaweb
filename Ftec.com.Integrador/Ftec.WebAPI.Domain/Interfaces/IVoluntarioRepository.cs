﻿using Ftec.WebAPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Domain.Interfaces
{
	public interface IVoluntarioRepository
	{
		Guid Insert(Voluntario voluntario);
		Voluntario Find(Guid id);
		List<Voluntario> FindAll();
		Guid Update(Voluntario voluntario);
		bool Delete(Guid id);
	}
}
