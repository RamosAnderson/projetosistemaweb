﻿using Ftec.WebAPI.Application.Adapter;
using Ftec.WebAPI.Application.DTO;
using Ftec.WebAPI.Domain.Entities;
using Ftec.WebAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Application
{
	public class AcaoApplication
	{
		IAcaoRepository acaoRepository;

		public AcaoApplication(IAcaoRepository acaoRepository)
		{
			this.acaoRepository = acaoRepository;
		}

		public Guid Insert(AcaoDTO AcaoDTO)
		{
			var acao = AcaoAdapter.ToDomain(AcaoDTO);
			return acaoRepository.Insert(acao);
		}

		public Guid Update(AcaoDTO AcaoDTO)
		{
			var acao = AcaoAdapter.ToDomain(AcaoDTO);
			return acaoRepository.Update(acao);
		}

		public bool Delete(Guid id)
		{
			return acaoRepository.Delete(id);
		}

		public AcaoDTO Get(Guid id)
		{
			var acao = acaoRepository.Find(id);
			return AcaoAdapter.ToDTO(acao);
		}

		public List<AcaoDTO> GetAll()
		{
			List<AcaoDTO> acoesDto = new List<AcaoDTO>();
			var acoes = acaoRepository.FindAll();

			foreach (Acao action in acoes)
			{
				acoesDto.Add(AcaoAdapter.ToDTO(action));
			}

			return acoesDto;
		}
	}
}
