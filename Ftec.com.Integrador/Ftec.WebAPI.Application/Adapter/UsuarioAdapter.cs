﻿using Ftec.WebAPI.Application.DTO;
using Ftec.WebAPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Application.Adapter
{
	public static class UsuarioAdapter
	{
		public static UsuarioDTO ToDTO(Usuario usuario)
		{
			return new UsuarioDTO()
			{
				Id    = usuario.Id,
				Nome  = usuario.Nome,
				Email = usuario.Email,
				Senha = usuario.Senha
			};
		}

		public static Usuario ToDomain(UsuarioDTO usuario)
		{
			return new Usuario()
			{
				Id    = usuario.Id,
				Nome  = usuario.Nome,
				Email = usuario.Email,
				Senha = usuario.Senha
			};
		}
	}


}
