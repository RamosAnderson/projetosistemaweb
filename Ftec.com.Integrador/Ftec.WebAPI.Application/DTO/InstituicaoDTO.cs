﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Application.DTO
{
	public class InstituicaoDTO
	{
		public Guid Id { get; set; }
		public string Razao { get; set; }
		public string Email { get; set; }
		public string Senha { get; set; }
		public string Cnpj { get; set; }
		public Boolean Ativo { get; set; }
		public EnderecoDTO Endereco { get; set; }
		public List<DoacaoDTO> Doacoes { get; set; }
	}
}
