﻿using Ftec.WebAPI.Domain.Entities;
using Ftec.WebAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace Ftec.WebAPI.Infra.PsqlRepository
{
	public class InstituicaoRepository : IInstituicaoRepository
	{
		private string connectionString;
		public InstituicaoRepository(string connectionString)
		{
			this.connectionString = connectionString;
		}

		public bool Delete(Guid id)
		{
			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();

				using (var trans = con.BeginTransaction())
				{
					try
					{
						NpgsqlCommand cmd = criarComando(con);

						cmd.Transaction = trans;
						cmd.CommandText = @"DELETE FROM Instituicao WHERE Id=@Id";
						cmd.Parameters.AddWithValue("Id", id);
						cmd.ExecuteNonQuery();

						trans.Commit();
						return true;
					}
					catch (Exception ex)
					{
						trans.Rollback();
						throw ex;
					}
				}
			}
		}

		public Instituicao Find(Guid id)
		{
			Instituicao instituicao = null;

			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();

				NpgsqlCommand cmd = criarComando(con);

				cmd.CommandText = @"SELECT * FROM Instituicao WHERE Id=@Id";
				cmd.Parameters.AddWithValue("Id", id.ToString());

				var reader = cmd.ExecuteReader();

				while (reader.Read())
				{
					instituicao = new Instituicao();
					instituicao.Id = Guid.Parse(reader["Id"].ToString());
					instituicao.Razao = reader["Razao"].ToString();
					instituicao.Email = reader["Email"].ToString();
					instituicao.Senha = reader["Senha"].ToString();
					instituicao.Ativo = Boolean.Parse(reader["Ativo"].ToString());
					instituicao.Cnpj = reader["Cnpj"].ToString();
					
				}

				reader.Close();
			}

			return instituicao;			
		}

		public List<Instituicao> FindAll()
		{
			List<Instituicao> instituicoes = new List<Instituicao>();

			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();

				NpgsqlCommand cmd = criarComando(con);

				cmd.CommandText = @"SELECT * FROM Instituicao";
				var reader = cmd.ExecuteReader();

				while (reader.Read())
				{
					Instituicao instituicao = new Instituicao();
					instituicao.Id = Guid.Parse(reader["Id"].ToString());
					instituicao.Razao = reader["Razao"].ToString();
					instituicao.Email = reader["Email"].ToString();
					instituicao.Senha = reader["Senha"].ToString();
					instituicao.Ativo = Boolean.Parse(reader["Ativo"].ToString());
					instituicao.Cnpj = reader["Cnpj"].ToString();

					instituicoes.Add(instituicao);
				}

				reader.Close();				
			}

			return instituicoes;
		}

		public Guid Insert(Instituicao instituicao)
		{
			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();
				//Inicia a transação
				using (var trans = con.BeginTransaction())
				{
					try
					{
						NpgsqlCommand cmd = criarComando(con);
						cmd.Transaction = trans;
						cmd.CommandText = @"INSERT Into Instituicao (Id,Razao,Email,Senha,Ativo,Cnpj) values (@Id,@Razao,@Email,@Senha,@Ativo,@Cnpj)";
						cmd.Parameters.AddWithValue("Id", instituicao.Id);
						cmd.Parameters.AddWithValue("Razao", instituicao.Razao);
						cmd.Parameters.AddWithValue("Email", instituicao.Email);
						cmd.Parameters.AddWithValue("Senha", instituicao.Senha);
						cmd.Parameters.AddWithValue("Ativo", instituicao.Ativo);
						cmd.Parameters.AddWithValue("Cnpj", instituicao.Cnpj);
						cmd.ExecuteNonQuery();

						trans.Commit();
						return instituicao.Id;
					}
					catch (Exception ex)
					{
						trans.Rollback();
						throw ex;
					}
				}
			}
		}

		public Guid Update(Instituicao instituicao)
		{
			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();
				//Inicia a transação
				using (var trans = con.BeginTransaction())
				{
					try
					{
						NpgsqlCommand cmd = criarComando(con);
						cmd.Transaction = trans;
						cmd.CommandText = @"UPDATE Instituicao SET Id=@Id,Razao=@Razao,Email=@Email,Senha=@Senha,Ativo=@Ativo,Cnpj=@Cnpj";
						cmd.Parameters.AddWithValue("Id", instituicao.Id);
						cmd.Parameters.AddWithValue("Razao", instituicao.Razao);
						cmd.Parameters.AddWithValue("Email", instituicao.Email);
						cmd.Parameters.AddWithValue("Senha", instituicao.Senha);
						cmd.Parameters.AddWithValue("Ativo", instituicao.Ativo);
						cmd.Parameters.AddWithValue("Cnpj", instituicao.Cnpj);
						cmd.ExecuteNonQuery();

						trans.Commit();
						return instituicao.Id;
					}
					catch (Exception ex)
					{
						trans.Rollback();
						throw ex;
					}
				}
			}
		}

		// MÉTODOS PRIVADOS DO REPOSITORY
		private NpgsqlCommand criarComando(NpgsqlConnection con)
		{
			NpgsqlCommand cmd = new NpgsqlCommand();

			cmd.Connection = con;

			return cmd;
		}
	}
}
