﻿using Ftec.WebAPI.Domain.Entities;
using Ftec.WebAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Infra.Repository
{
	public class AcaoRepository : IAcaoRepository
	{
		private string connectionString;

		public AcaoRepository(string connectionString)
		{
			this.connectionString = connectionString;
		}

		public bool Delete(Guid id)
		{
			throw new NotImplementedException();
		}

		public Acao Find(Guid id)
		{
			throw new NotImplementedException();
		}

		public List<Acao> FindAll()
		{
			throw new NotImplementedException();
		}

		public Guid Insert(Acao acao)
		{
			throw new NotImplementedException();
		}

		public Guid Update(Acao acao)
		{
			throw new NotImplementedException();
		}
	}
}
