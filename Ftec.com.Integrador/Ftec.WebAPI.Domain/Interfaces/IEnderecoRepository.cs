﻿using Ftec.WebAPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Domain.Interfaces
{
	public interface IEnderecoRepository
	{
		Guid Insert(Endereco endereco);
		Endereco Find(Guid id);
		List<Endereco> FindAll();
		Guid Update(Endereco endereco);
		bool Delete(Guid id);
	}
}
