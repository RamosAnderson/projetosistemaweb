﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Domain.Entities
{
	public class Acao
	{
		public Guid Id { get; set; }
		public string Descricao { get; set; }
		public int Participantes { get; set; }
		public DateTime DataHora { get; set; }
		public TipoAcao TipoAcao { get; set; }
		public Endereco Endereco { get; set; }
	}
}
