﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Application.DTO
{
	public class AcaoDTO
	{
		public Guid Id { get; set; }
		public string Descricao { get; set; }
		public int Participantes { get; set; }
		public DateTime DataHora { get; set; }
		public TipoAcaoDTO TipoAcao { get; set; }
		public EnderecoDTO Endereco { get; set; }
	}
}
