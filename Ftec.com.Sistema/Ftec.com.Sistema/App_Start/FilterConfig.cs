﻿using System.Web;
using System.Web.Mvc;

namespace Ftec.com.Sistema
{
	public class FilterConfig
	{
		public static void RegisterGlobalFilters(GlobalFilterCollection filters)
		{
			filters.Add(new HandleErrorAttribute());
		}
	}
}
