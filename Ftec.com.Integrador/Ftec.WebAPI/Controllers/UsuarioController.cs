﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Ftec.WebAPI.Application;
using Ftec.WebAPI.Application.DTO;
using Ftec.WebAPI.Domain.Entities;
using Ftec.WebAPI.Domain.Interfaces;
using Ftec.WebAPI.Infra.Repository;

namespace Ftec.WebAPI.Controllers
{
	public class UsuarioController : ApiController
	{
		private IUsuarioRepository usuarioRepository;
		private UsuarioApplication usuarioApplication;

		public UsuarioController()
		{
			string connectionString = string.Empty;

			usuarioRepository   = new Ftec.WebAPI.Infra.PsqlRepository.UsuarioRepository(connectionString);
			usuarioApplication = new UsuarioApplication(usuarioRepository);
		}

		public HttpResponseMessage Get(Guid id)
		{
			try
			{
				Usuario user = Procurar(id);
				if (user == null)
				{
					return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Usuário não encontrado");
				}
				else
				{
					return Request.CreateResponse(HttpStatusCode.OK, user);
				}

			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		public HttpResponseMessage Post([FromBody] Usuario usuario)
		{
			try
			{
				Guid id = Inserir(usuario);
				return Request.CreateResponse(HttpStatusCode.OK, id);
			}
			catch (ApplicationException ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		public HttpResponseMessage Put(Guid id, [FromBody]Usuario usuario)
		{
			try
			{
				Usuario user = Procurar(id);
				if (user == null)
				{
					return Request.CreateResponse(HttpStatusCode.NotFound, "Usuário não encontrado");
				}
				else
				{
					Alterar(usuario);
					return Request.CreateResponse(HttpStatusCode.OK, id);
				}
			}
			catch (ApplicationException ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}	

		public HttpResponseMessage Delete(Guid id)
		{
			try
			{
				Usuario user = Procurar(id);
				if (user == null)
				{
					return Request.CreateResponse(HttpStatusCode.NotFound, "Usuário a ser excluido não encontrado");
				}
				else
				{
					Excluir(id);
					return Request.CreateResponse(HttpStatusCode.OK, id);
				}
			}
			catch (ApplicationException ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}

		}

		private Usuario Procurar(Guid id)
		{
			var usuarioDTO = usuarioApplication.Get(id);

			return new Usuario()
			{
				Id    = usuarioDTO.Id,
				Nome  = usuarioDTO.Nome,
				Email = usuarioDTO.Email,
				Senha = usuarioDTO.Senha
			};
		}

		private List<Usuario> ProcurarTodos()
		{
			var usuarios = new List<Usuario>();
			var usuariosDTO = usuarioApplication.GetAll();

			foreach(UsuarioDTO usuarioDTO in usuariosDTO)
			{
				usuarios.Add(new Usuario()
				{
					Id = usuarioDTO.Id,
					Nome = usuarioDTO.Nome,
					Email = usuarioDTO.Email,
					Senha = usuarioDTO.Senha
				});
			}

			return usuarios;
		}

		private Guid Inserir(Usuario usuario)
		{
			UsuarioDTO usuarioDTO = new UsuarioDTO()
			{
				Id = usuario.Id,
				Nome = usuario.Nome,
				Email = usuario.Email,
				Senha = usuario.Senha
			};

			return usuarioApplication.Insert(usuarioDTO);
		}

		private void Alterar(Usuario usuario)
		{
			UsuarioDTO userDTO = new UsuarioDTO()
			{
				Id    = usuario.Id,
				Nome  = usuario.Nome,
				Email = usuario.Email,
				Senha = usuario.Senha
			};

			usuarioApplication.Update(userDTO);
		}

		private void Excluir(Guid id)
		{
			usuarioApplication.Delete(id);
		}
    }
}
