﻿using Ftec.WebAPI.Domain.Entities;
using Ftec.WebAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace Ftec.WebAPI.Infra.PsqlRepository
{
	public class VoluntarioRepository : IVoluntarioRepository
	{
		private string connectionString;
		public VoluntarioRepository(string connectionString)
		{
			this.connectionString = connectionString;
		}
		public bool Delete(Guid id)
		{
			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();

				using (var trans = con.BeginTransaction())
				{
					try
					{
						NpgsqlCommand cmd = criarComando(con);

						cmd.Transaction = trans;
						cmd.CommandText = @"DELETE FROM Voluntario WHERE Id=@Id";
						cmd.Parameters.AddWithValue("Id", id);
						cmd.ExecuteNonQuery();

						trans.Commit();
						return true;
					}
					catch (Exception ex)
					{
						trans.Rollback();
						throw ex;
					}
				}
			}
		}

		public Voluntario Find(Guid id)
		{
			Voluntario voluntario = null;

			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();

				NpgsqlCommand cmd = criarComando(con);

				cmd.CommandText = @"SELECT * FROM Voluntario WHERE Id=@Id";
				cmd.Parameters.AddWithValue("Id", id.ToString());

				var reader = cmd.ExecuteReader();

				while (reader.Read())
				{
					voluntario = new Voluntario();
					voluntario.Id = Guid.Parse(reader["Id"].ToString());
					voluntario.Nome = reader["Nome"].ToString();
					voluntario.Email = reader["Email"].ToString();
					voluntario.Senha = reader["Senha"].ToString();
					voluntario.Ativo = bool.Parse(reader["Ativo"].ToString()); 
					voluntario.Cpf = reader["Cpf"].ToString();
					voluntario.Telefone = reader["Telefone"].ToString();
				}

				reader.Close();
			}

			return voluntario;
		}

		public List<Voluntario> FindAll()
		{
			List<Voluntario> voluntarios = new List<Voluntario>();

			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();

				NpgsqlCommand cmd = criarComando(con);

				cmd.CommandText = @"SELECT * FROM Voluntario";
				var reader = cmd.ExecuteReader();

				while (reader.Read())
				{
					Voluntario voluntario = new Voluntario();
					voluntario.Id = Guid.Parse(reader["Id"].ToString());
					voluntario.Nome = reader["Nome"].ToString();
					voluntario.Email = reader["Email"].ToString();
					voluntario.Senha = reader["Senha"].ToString();
					voluntario.Ativo = bool.Parse(reader["Ativo"].ToString());
					voluntario.Cpf = reader["Cpf"].ToString();
					voluntario.Telefone = reader["Telefone"].ToString();

					voluntarios.Add(voluntario);
				}

				reader.Close();
			}

			return voluntarios;
		}

		public Guid Insert(Voluntario voluntario)
		{
			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();
				//Inicia a transação
				using (var trans = con.BeginTransaction())
				{
					try
					{
						NpgsqlCommand cmd = criarComando(con);
						cmd.Transaction = trans;
						cmd.CommandText = @"INSERT Into Voluntario (Id,Nome,Email,Senha,Ativo,Telefone) values (@Id,@Nome,@Email,@Senha,@Ativo,@Telefone)";
						cmd.Parameters.AddWithValue("Id", voluntario.Id);
						cmd.Parameters.AddWithValue("Nome", voluntario.Nome);
						cmd.Parameters.AddWithValue("Email", voluntario.Email);
						cmd.Parameters.AddWithValue("Senha", voluntario.Senha);
						cmd.Parameters.AddWithValue("Ativo", voluntario.Ativo);
						cmd.Parameters.AddWithValue("Telefone", voluntario.Telefone);
						cmd.ExecuteNonQuery();

						trans.Commit();
						return voluntario.Id;
					}
					catch (Exception ex)
					{
						trans.Rollback();
						throw ex;
					}
				}
			}
		}

		public Guid Update(Voluntario voluntario)
		{
			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();
				//Inicia a transação
				using (var trans = con.BeginTransaction())
				{
					try
					{
						NpgsqlCommand cmd = criarComando(con);
						cmd.Transaction = trans;
						cmd.CommandText = @"UPDATE Voluntario SET Id=@Id,Nome=@Nome,Email=@Email,Senha=@Senha,Ativo=@Ativo,Telefone=@Telefone";
						cmd.Parameters.AddWithValue("Id", voluntario.Id);
						cmd.Parameters.AddWithValue("Nome", voluntario.Nome);
						cmd.Parameters.AddWithValue("Email", voluntario.Email);
						cmd.Parameters.AddWithValue("Senha", voluntario.Senha);
						cmd.Parameters.AddWithValue("Ativo", voluntario.Ativo);
						cmd.Parameters.AddWithValue("Telefone", voluntario.Telefone);
						cmd.ExecuteNonQuery();

						trans.Commit();
						return voluntario.Id;
					}
					catch (Exception ex)
					{
						trans.Rollback();
						throw ex;
					}
				}
			}
		}

		// MÉTODOS PRIVADOS DO REPOSITORY
		private NpgsqlCommand criarComando(NpgsqlConnection con)
		{
			NpgsqlCommand cmd = new NpgsqlCommand();

			cmd.Connection = con;

			return cmd;
		}
	}
}
