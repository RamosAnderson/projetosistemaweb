﻿using Ftec.WebAPI.Domain.Entities;
using Ftec.WebAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Infra.Repository
{
	public class InstituicaoRepository : IInstituicaoRepository
	{
		private string connectionString;

		public InstituicaoRepository(string connectionString)
		{
			this.connectionString = connectionString;
		}

		public bool Delete(Guid id)
		{
			throw new NotImplementedException();
		}

		public Instituicao Find(Guid id)
		{
			throw new NotImplementedException();
		}

		public List<Instituicao> FindAll()
		{
			throw new NotImplementedException();
		}

		public Guid Insert(Instituicao instituicao)
		{
			throw new NotImplementedException();
		}

		public Guid Update(Instituicao instituicao)
		{
			throw new NotImplementedException();
		}
	}
}
