﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Ftec.com.Administracao.Models
{
	public class Acao
	{
		public Guid Id { get; set; }
		[Required(ErrorMessage = "Descrição deve ser informada!")]
		public string Descricao { get; set; }
		public Boolean Ativo { get; set; }

		public Acao()
		{
			Id = Guid.NewGuid();
		}
	}
}