﻿using Ftec.WebAPI.Application.DTO;
using Ftec.WebAPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Application.Adapter
{
	public static class InstituicaoAdapter
	{
		public static InstituicaoDTO ToDTO(Instituicao instituicao)
		{
			var inst = new InstituicaoDTO()
			{
				Id = instituicao.Id,
				Senha = instituicao.Senha,
				Razao = instituicao.Razao,
				Cnpj = instituicao.Cnpj,
				Ativo = instituicao.Ativo,
				Email = instituicao.Email,
				Endereco = new EnderecoDTO()
				{
					Id = instituicao.Endereco.Id,
					Rua = instituicao.Endereco.Rua,
					Cep = instituicao.Endereco.Cep,
					Complemento = instituicao.Endereco.Complemento,
					Bairro = instituicao.Endereco.Bairro,
					Numero = instituicao.Endereco.Numero,
					Cidade = instituicao.Endereco.Cidade
				}
			};

			foreach (Doacao doacao in instituicao.Doacoes)
			{
				var doc = new DoacaoDTO()
				{
					Id = doacao.Id,
					Descricao = doacao.Descricao,
					Disponivel = doacao.Disponivel,
					Valor = doacao.Valor,
					Tipo = new TipoDoacaoDTO()
					{
						Id = doacao.Tipo.Id,
						Codigo = doacao.Tipo.Codigo,
						Descricao = doacao.Descricao
					}
				};

				inst.Doacoes.Add(doc);
			}

			return inst;
		}

		public static Instituicao ToDomain(InstituicaoDTO instituicao)
		{
			var inst = new Instituicao()
			{
				Id = instituicao.Id,
				Senha = instituicao.Senha,
				Razao = instituicao.Razao,
				Cnpj = instituicao.Cnpj,
				Ativo = instituicao.Ativo,
				Email = instituicao.Email,
				Endereco = new Endereco()
				{
					Id = instituicao.Endereco.Id,
					Rua = instituicao.Endereco.Rua,
					Cep = instituicao.Endereco.Cep,
					Complemento = instituicao.Endereco.Complemento,
					Bairro = instituicao.Endereco.Bairro,
					Numero = instituicao.Endereco.Numero,
					Cidade = instituicao.Endereco.Cidade
				}
			};

			foreach (DoacaoDTO doacao in instituicao.Doacoes)
			{
				var doc = new Doacao()
				{
					Id = doacao.Id,
					Descricao = doacao.Descricao,
					Disponivel = doacao.Disponivel,
					Valor = doacao.Valor,
					Tipo = new TipoDoacao()
					{
						Id = doacao.Tipo.Id,
						Codigo = doacao.Tipo.Codigo,
						Descricao = doacao.Descricao
					}
				};

				inst.Doacoes.Add(doc);
			}

			return inst;
		}
	}
}
