﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Application.DTO
{
	public class TipoDoacaoDTO
	{
		public Guid Id { get; set; }
		public int Codigo { get; set; }
		public string Descricao { get; set; }
	}
}
