﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ftec.com.Sistema.Models
{
	public class TipoAcao
	{	
		public Guid Id { get; set; }
		public int Codigo { get; set; }
		public String Descricao { get; set; }

		public TipoAcao()
		{
			Id = Guid.NewGuid();
		}
	}
}