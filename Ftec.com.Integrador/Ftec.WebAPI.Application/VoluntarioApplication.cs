﻿using Ftec.WebAPI.Application.Adapter;
using Ftec.WebAPI.Application.DTO;
using Ftec.WebAPI.Domain.Entities;
using Ftec.WebAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Application
{
	public class VoluntarioApplication
	{
		IVoluntarioRepository voluntarioRepository;

		public VoluntarioApplication(IVoluntarioRepository voluntarioRepository)
		{
			this.voluntarioRepository = voluntarioRepository;
		}

		public Guid Insert(VoluntarioDTO voluntarioDTO)
		{
			var voluntario = VoluntarioAdapter.ToDomain(voluntarioDTO);
			return voluntarioRepository.Insert(voluntario);
		}

		public Guid Update(VoluntarioDTO voluntarioDTO)
		{
			var voluntario = VoluntarioAdapter.ToDomain(voluntarioDTO);
			return voluntarioRepository.Update(voluntario);
		}

		public bool Delete(Guid id)
		{
			return voluntarioRepository.Delete(id);
		}

		public VoluntarioDTO Get(Guid id)
		{
			var voluntario = voluntarioRepository.Find(id);
			return VoluntarioAdapter.ToDTO(voluntario);
		}

		public List<VoluntarioDTO> GetAll()
		{
			List<VoluntarioDTO> voluntariosDto = new List<VoluntarioDTO>();
			var voluntarios = voluntarioRepository.FindAll();

			foreach (Voluntario volunt in voluntarios)
			{
				voluntariosDto.Add(VoluntarioAdapter.ToDTO(volunt));
			}

			return voluntariosDto;
		}
	}
}
