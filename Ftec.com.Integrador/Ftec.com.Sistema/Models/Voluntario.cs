﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Ftec.com.Sistema.Models
{
	public class Voluntario
	{
		public Guid Id { get; set; }
		[Required(ErrorMessage = "O Nome deve ser informado")]
		public string Nome { get; set; }
		[Required(ErrorMessage = "O E-mail deve ser informado")]
		public string Email { get; set; }
		[Required(ErrorMessage = "Uma senha deve ser informada")]
		public string Senha { get; set; }		
		public bool Ativo { get; set; }
		[Required(ErrorMessage = "O Cpf deve ser informado")]
		public string Cpf { get; set; }
		public Endereco Endereco { get; set; }
		[Required(ErrorMessage = "O Telefone deve ser informado")]
		public string Telefone { get; set; }

		public List<TipoAcao> Afinidade { get; set; }

		public Voluntario()
		{
			Id = Guid.NewGuid();
			Endereco = new Endereco();
		}
	}
}