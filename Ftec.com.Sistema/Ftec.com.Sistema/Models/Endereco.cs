﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ftec.com.Sistema.Models
{
	public class Endereco
	{
		public Guid   Id		  { get; set; }
		public string Cep	      { get; set; }
		public string Rua	      { get; set; }
		public string Numero	  { get; set; }
		public string Complemento { get; set; }
		public string Bairro      { get; set; }
		public string Cidade      { get; set; }

		public Endereco()
		{
			Id = Guid.NewGuid();
		}
	}
}