﻿using Ftec.WebAPI.Application.Adapter;
using Ftec.WebAPI.Application.DTO;
using Ftec.WebAPI.Domain.Entities;
using Ftec.WebAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Application
{
    public class UsuarioApplication
    {
		IUsuarioRepository usuarioRepository;

		public UsuarioApplication(IUsuarioRepository usuarioRepository)
		{
			this.usuarioRepository = usuarioRepository;
		}

		public Guid Insert(UsuarioDTO usuarioDto)
		{
			var usuario = UsuarioAdapter.ToDomain(usuarioDto);
			return usuarioRepository.Insert(usuario);
		}

		public Guid Update(UsuarioDTO usuarioDto)
		{
			var usuario = UsuarioAdapter.ToDomain(usuarioDto);
			return usuarioRepository.Update(usuario);
		}

		public bool Delete(Guid id)
		{
			return usuarioRepository.Delete(id);
		}

		public UsuarioDTO Get(Guid id)
		{
			var usuario = usuarioRepository.Find(id);
			return UsuarioAdapter.ToDTO(usuario);
		}

		public List<UsuarioDTO> GetAll()
		{
			List<UsuarioDTO> usuariosDto = new List<UsuarioDTO>();
			var usuarios = usuarioRepository.FindAll();

			foreach(Usuario user in usuarios)
			{
				usuariosDto.Add(UsuarioAdapter.ToDTO(user));
			}

			return usuariosDto;
		}
	}
}
