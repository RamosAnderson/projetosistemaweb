﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Application.DTO
{
	public class VoluntarioDTO
	{
		public Guid Id { get; set; }

		public string Nome { get; set; }

		public string Email { get; set; }

		public string Senha { get; set; }
		public bool Ativo { get; set; }

		public string Cpf { get; set; }

		public EnderecoDTO Endereco { get; set; }
		public string Telefone { get; set; }

		public List<TipoAcaoDTO> Afinidade { get; set; }
	}
}
