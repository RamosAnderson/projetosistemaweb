﻿using Ftec.com.Sistema.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ftec.com.Sistema.Controllers
{
    public class TipoDoacaoController : Controller
    {
        // GET: TipoDoacao
        public ActionResult Index()
        {
			List<TipoDoacao> tipos = getListaTipoDoacoes();

			return View(tipos);
        }

		public ActionResult Novo()
		{
			return View();
		}

		public ActionResult ProcessarGravacaoPost(TipoDoacao tpDoacao)
		{
			var tipos = getListaTipoDoacoes();
			tipos.Add(tpDoacao);
			Session["tpDoacoes"] = tipos;


			return RedirectToAction("Index");
		}

		private List<TipoDoacao> getListaTipoDoacoes()
		{
			List<TipoDoacao> tpDoacoes;
			if (Session["tpDoacoes"] == null)
			{
				tpDoacoes = new List<TipoDoacao>();
			}
			else
			{
				tpDoacoes = (List<TipoDoacao>)Session["tpDoacoes"];

			}

			return tpDoacoes;
		}
    }
}