﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Ftec.com.Administracao.Models
{
	public class Voluntario
	{
		public Guid    Id	 { get; set; }
		[Required(ErrorMessage = "O Nome deve ser informado")]
		public string  Nome	 { get; set; }
		public string  Email { get; set; }
		public string  Senha { get; set; }
		public bool    Ativo { get; set; }
		
		// Lembrar de incluir um array list de endereço

		public Voluntario()
		{
			Id = Guid.NewGuid();
		}

	}
}