﻿using Ftec.WebAPI.Application.DTO;
using Ftec.WebAPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Application.Adapter
{
	public static class AcaoAdapter
	{
		public static AcaoDTO ToDTO(Acao acao)
		{
			return new AcaoDTO()
			{
				Id = acao.Id,
				DataHora = acao.DataHora,
				Descricao = acao.Descricao,
				Participantes = acao.Participantes,
				TipoAcao = new TipoAcaoDTO(){
					Id = acao.TipoAcao.Id,
					Codigo = acao.TipoAcao.Codigo,
					Descricao = acao.TipoAcao.Descricao
				},
				Endereco = new EnderecoDTO()
				{
					Id = acao.Endereco.Id,
					Rua = acao.Endereco.Rua,
					Cep = acao.Endereco.Cep,
					Complemento = acao.Endereco.Complemento,
					Bairro = acao.Endereco.Bairro,
					Numero = acao.Endereco.Numero,
					Cidade = acao.Endereco.Cidade
				}
			};
		}

		public static Acao ToDomain(AcaoDTO acao)
		{
			return new Acao()
			{
				Id = acao.Id,
				DataHora = acao.DataHora,
				Descricao = acao.Descricao,
				Participantes = acao.Participantes,
				TipoAcao = new TipoAcao()
				{
					Id = acao.TipoAcao.Id,
					Codigo = acao.TipoAcao.Codigo,
					Descricao = acao.TipoAcao.Descricao
				},
				Endereco = new Endereco()
				{
					Id = acao.Endereco.Id,
					Rua = acao.Endereco.Rua,
					Cep = acao.Endereco.Cep,
					Complemento = acao.Endereco.Complemento,
					Bairro = acao.Endereco.Bairro,
					Numero = acao.Endereco.Numero,
					Cidade = acao.Endereco.Cidade
				}
			};
		}
	}
}
