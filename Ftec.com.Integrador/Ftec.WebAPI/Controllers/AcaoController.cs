﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Ftec.WebAPI.Application;
using Ftec.WebAPI.Application.DTO;
using Ftec.WebAPI.Domain.Interfaces;
using Ftec.WebAPI.Infra.Repository;
using Ftec.WebAPI.Models;


namespace Ftec.WebAPI.Controllers
{
	public class AcaoController : ApiController
	{
		private IAcaoRepository acaoRepository;
		private AcaoApplication acaoApplication;

		public AcaoController()
		{
			string connectionString = string.Empty;

			acaoRepository = new Ftec.WebAPI.Infra.PsqlRepository.AcaoRepository(connectionString);
			acaoApplication = new AcaoApplication(acaoRepository);
		}

		public HttpResponseMessage Get()
		{
			try
			{
				List<Acao> acoes = ProcurarTodos();
				return Request.CreateResponse(HttpStatusCode.OK, acoes);
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		public HttpResponseMessage Get(Guid id)
		{
			try
			{
				Acao acao = Procurar(id);
				if (acao == null)
				{
					return Request.CreateResponse(HttpStatusCode.NotFound, "Registro não encontrado!");
				}
				else
				{
					return Request.CreateResponse(HttpStatusCode.OK, acao);
				}
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		public HttpResponseMessage Post([FromBody]Acao acao)
		{
			try
			{
				Guid id = Inserir(acao);
				return Request.CreateResponse(HttpStatusCode.OK, id);
			}
			catch (ApplicationException ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		public HttpResponseMessage Put(Guid id, [FromBody]Acao acao)
		{
			try
			{
				Acao action = Procurar(id);
				if (action == null)
				{
					return Request.CreateResponse(HttpStatusCode.NotFound, "Acao a ser alterada não foi encontrada!");
				}
				else
				{
					Alterar(acao);
					return Request.CreateResponse(HttpStatusCode.OK, id);
				}
			}
			catch (ApplicationException ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		public HttpResponseMessage Delete(Guid id)
		{
			try
			{
				Acao acao = Procurar(id);
				if (acao == null)
				{
					return Request.CreateResponse(HttpStatusCode.NotFound, "Acao a ser excluida não encontrada!");
				}
				else
				{
					Excluir(id);
					return Request.CreateResponse(HttpStatusCode.OK, id);
				}
			}
			catch (ApplicationException ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		/*MÉTODOS PRIVADOS DO CONTROLLER*/
		private List<Acao> ProcurarTodos()
		{
			var acoes = new List<Acao>();
			var acoesDTO = acaoApplication.GetAll();

			foreach (AcaoDTO acaoDTO in acoesDTO)
			{
				acoes.Add(new Acao()
				{
					Id = acaoDTO.Id,
					Descricao = acaoDTO.Descricao,
					DataHora = acaoDTO.DataHora,
					Participantes = acaoDTO.Participantes,
					Endereco = new Endereco()
					{
						Id = acaoDTO.Endereco.Id,
						Bairro = acaoDTO.Endereco.Bairro,
						Cep = acaoDTO.Endereco.Cep,
						Cidade = acaoDTO.Endereco.Cidade,
						Complemento = acaoDTO.Endereco.Complemento,
						Numero = acaoDTO.Endereco.Numero,
						Rua = acaoDTO.Endereco.Rua
					},
					TipoAcao = new TipoAcao()
					{
						Id = acaoDTO.TipoAcao.Id,
						Codigo = acaoDTO.TipoAcao.Codigo,
						Descricao = acaoDTO.TipoAcao.Descricao
					}
				});
			}

			return acoes;

		}

		private Acao Procurar(Guid id)
		{
			var acaoDTO = acaoApplication.Get(id);

			return new Acao()
			{
				Id = acaoDTO.Id,
				Descricao = acaoDTO.Descricao,
				DataHora = acaoDTO.DataHora,
				Participantes = acaoDTO.Participantes,
				Endereco = new Endereco()
				{
					Id = acaoDTO.Endereco.Id,
					Bairro = acaoDTO.Endereco.Bairro,
					Cep = acaoDTO.Endereco.Cep,
					Cidade = acaoDTO.Endereco.Cidade,
					Complemento = acaoDTO.Endereco.Complemento,
					Numero = acaoDTO.Endereco.Numero,
					Rua = acaoDTO.Endereco.Rua
				},
				TipoAcao = new TipoAcao()
				{
					Id = acaoDTO.TipoAcao.Id,
					Codigo = acaoDTO.TipoAcao.Codigo,
					Descricao = acaoDTO.TipoAcao.Descricao
				}
			};
		}

		private Guid Inserir(Acao acao)
		{
			AcaoDTO acaoDTO = new AcaoDTO()
			{
				Id = acao.Id,
				Descricao = acao.Descricao,
				DataHora = acao.DataHora,
				Participantes = acao.Participantes,
				Endereco = new EnderecoDTO()
				{
					Id = acao.Endereco.Id,
					Bairro = acao.Endereco.Bairro,
					Cep = acao.Endereco.Cep,
					Cidade = acao.Endereco.Cidade,
					Complemento = acao.Endereco.Complemento,
					Numero = acao.Endereco.Numero,
					Rua = acao.Endereco.Rua
				},
				TipoAcao = new TipoAcaoDTO()
				{
					Id = acao.TipoAcao.Id,
					Codigo = acao.TipoAcao.Codigo,
					Descricao = acao.TipoAcao.Descricao
				}
			};

			return acaoApplication.Insert(acaoDTO);
		}

		private void Alterar(Acao acao)
		{
			AcaoDTO acaoDTO = new AcaoDTO()
			{
				Id = acao.Id,
				Descricao = acao.Descricao,
				DataHora = acao.DataHora,
				Participantes = acao.Participantes,
				Endereco = new EnderecoDTO()
				{
					Id = acao.Endereco.Id,
					Bairro = acao.Endereco.Bairro,
					Cep = acao.Endereco.Cep,
					Cidade = acao.Endereco.Cidade,
					Complemento = acao.Endereco.Complemento,
					Numero = acao.Endereco.Numero,
					Rua = acao.Endereco.Rua
				},
				TipoAcao = new TipoAcaoDTO()
				{
					Id = acao.TipoAcao.Id,
					Codigo = acao.TipoAcao.Codigo,
					Descricao = acao.TipoAcao.Descricao
				}
			};

			acaoApplication.Update(acaoDTO);
		}

		private void Excluir(Guid id)
		{
			acaoApplication.Delete(id);
		}
	}
}