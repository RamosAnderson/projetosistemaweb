﻿using Ftec.WebAPI.Domain.Entities;
using Ftec.WebAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace Ftec.WebAPI.Infra.PsqlRepository
{
	public class TipoAcaoRepository : ITipoAcaoRepository
	{
		private string connectionString;
		public TipoAcaoRepository(string connectionString)
		{
			this.connectionString = connectionString;
		}

		public bool Delete(Guid id)
		{
			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();

				using (var trans = con.BeginTransaction())
				{
					try
					{
						NpgsqlCommand cmd = criarComando(con);

						cmd.Transaction = trans;
						cmd.CommandText = @"DELETE FROM TipoAcao WHERE Id=@Id";
						cmd.Parameters.AddWithValue("Id", id);
						cmd.ExecuteNonQuery();

						trans.Commit();
						return true;
					}
					catch (Exception ex)
					{
						trans.Rollback();
						throw ex;
					}
				}
			}
		}

		public TipoAcao Find(Guid id)
		{
			TipoAcao tpAcao = null;

			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();

				NpgsqlCommand cmd = criarComando(con);

				cmd.CommandText = @"SELECT * FROM TipoAcao WHERE Id=@Id";
				cmd.Parameters.AddWithValue("Id", id.ToString());

				var reader = cmd.ExecuteReader();

				while (reader.Read())
				{
					tpAcao = new TipoAcao();
					tpAcao.Id = Guid.Parse(reader["Id"].ToString());
					tpAcao.Codigo = int.Parse(reader["Codigo"].ToString());
					tpAcao.Descricao = reader["Descricao"].ToString();
					
				}

				reader.Close();
			}

			return tpAcao;			
		}

		public List<TipoAcao> FindAll()
		{
			List<TipoAcao> tpAcoes = new List<TipoAcao>();

			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();

				NpgsqlCommand cmd = criarComando(con);
				cmd.CommandText = @"SELECT * FROM TipoAcao";

				var reader = cmd.ExecuteReader();

				while (reader.Read())
				{
					TipoAcao tpAcao = new TipoAcao();
					tpAcao.Id = Guid.Parse(reader["Id"].ToString());
					tpAcao.Codigo = int.Parse(reader["Codigo"].ToString());
					tpAcao.Descricao = reader["Descricao"].ToString();

					tpAcoes.Add(tpAcao);
				}

				reader.Close();
			}
			return tpAcoes;
		}

		public Guid Insert(TipoAcao tpAcao)
		{
			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();
				//Inicia a transação
				using (var trans = con.BeginTransaction())
				{
					try
					{
						NpgsqlCommand cmd = criarComando(con);
						cmd.Transaction = trans;
						cmd.CommandText = @"INSERT INTO TipoAcao (Id,Codigo,Descricao) values (@Id,@Codigo,@Descricao)";
						cmd.Parameters.AddWithValue("Id", tpAcao.Id);
						cmd.Parameters.AddWithValue("Codigo", tpAcao.Codigo);
						cmd.Parameters.AddWithValue("Rua", tpAcao.Descricao);
						cmd.ExecuteNonQuery();

						trans.Commit();
						return tpAcao.Id;

					}
					catch (Exception ex)
					{
						trans.Rollback();
						throw ex;
					}
				}
			}
		}

		public Guid Update(TipoAcao tpAcao)
		{
			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();
				//Inicia a transação
				using (var trans = con.BeginTransaction())
				{
					try
					{
						NpgsqlCommand cmd = criarComando(con);
						cmd.Transaction = trans;
						cmd.CommandText = @"UPDATE TipoAcao SET Id=@Id,Codigo=@Codigo,Descricao=@Descricao";
						cmd.Parameters.AddWithValue("Id", tpAcao.Id);
						cmd.Parameters.AddWithValue("Codigo", tpAcao.Codigo);
						cmd.Parameters.AddWithValue("Rua", tpAcao.Descricao);
						cmd.ExecuteNonQuery();

						trans.Commit();
						return tpAcao.Id;

					}
					catch (Exception ex)
					{
						trans.Rollback();
						throw ex;
					}
				}
			}
		}

		// MÉTODOS PRIVADOS DO REPOSITORY
		private NpgsqlCommand criarComando(NpgsqlConnection con)
		{
			NpgsqlCommand cmd = new NpgsqlCommand();

			cmd.Connection = con;

			return cmd;
		}
	}
}
