﻿using Ftec.com.Administracao.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ftec.com.Administracao.Controllers
{
    public class UsuarioController : Controller
    {
        // GET: Usuario
        public ActionResult Index()
        {
			var usuarios = GetListaUsers();
            return View(usuarios);
        }
		
		public ActionResult Novo()
		{
			return View();
		}

		public ActionResult Editar(Guid Id)
		{
			var usuarios = GetListaUsers();
			var usuario = usuarios.FirstOrDefault(l => l.Id == Id);
			ViewBag.Usuario = usuario;

			return View();
		}

		public ActionResult Excluir(Guid Id)
		{
			var usuarios = GetListaUsers();
			var usuario = usuarios.FirstOrDefault(l => l.Id == Id);
			usuarios.Remove(usuario);

			Session["users"] = usuarios;

			return RedirectToAction("Index");
		}

		public ActionResult Login()
		{
			var listaUsuario = GetListaUsers();
			var novoUsuario   = new Usuario();
			novoUsuario.Email = "anderson@ftec.com";
			novoUsuario.Nome  = "Anderson Ramos";
			novoUsuario.Senha = "123";

			listaUsuario.Add(novoUsuario);
			Session["users"] = listaUsuario;
			
			return View();
		}	

		[HttpPost]
		public ActionResult ProcessarGravacaoPost(Usuario usuario)
		{
			if (ModelState.IsValid)
			{
				var usuarios = GetListaUsers();
				usuarios.Add(usuario);

				Session["users"] = usuarios;

				return RedirectToAction("Index");
			}
			else
			{
				return View("Novo", usuario);
			}
		}

		public ActionResult ProcessarAlteracaoPost(Usuario usuario)
		{
			var usuarios = GetListaUsers();
			var usuarioOld = usuarios.FirstOrDefault(l => l.Id == usuario.Id);

			usuarios.Remove(usuarioOld);
			usuarios.Add(usuario);

			Session["users"] = usuarios;

			return RedirectToAction("Index");
		}

		public ActionResult ProcessarLoginPost(Usuario usuario)
		{
			var usuarios = GetListaUsers();

			foreach (var user in usuarios)
			{
				if (usuario.Email == user.Email &&
					usuario.Senha == user.Senha)
				{
					Session["UsuarioLogado"] = user.Id;
					return RedirectToAction("Index", "Home");
				}
				else
				{
					if (usuarios.IndexOf(user) == usuarios.Count - 1)
					{
						return View("Usuário não encontrado!");
					}
				}
			}

			return View("Usuário não encontrado!");
		}

		public ActionResult ProcessarSaidaPost()
		{
			Session["UsuarioLogado"] = null;
			return RedirectToAction("Index", "Home");
		}

		public List<Usuario> GetListaUsers()
		{
			List<Usuario> usuarios;
			//
			if (Session["users"] == null)
			{
				usuarios = new List<Usuario>();
			}
			else
			{
				usuarios = (List<Usuario>)Session["users"];
			}

			return usuarios;
		}

	}
}