﻿using Ftec.WebAPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Domain.Interfaces
{
	public interface ITipoDoacaoRepository
	{
		Guid Insert(TipoDoacao tpDoacao);
		TipoDoacao Find(Guid id);
		List<TipoDoacao> FindAll();
		Guid Update(TipoDoacao tpDoacao);
		bool Delete(Guid id);
	}
}
