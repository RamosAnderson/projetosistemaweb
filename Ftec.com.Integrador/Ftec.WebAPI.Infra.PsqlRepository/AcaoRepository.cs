﻿using Ftec.WebAPI.Domain.Entities;
using Ftec.WebAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace Ftec.WebAPI.Infra.PsqlRepository
{
	public class AcaoRepository : IAcaoRepository
	{
		private string connectionString;

		public AcaoRepository(string connectionString)
		{
			this.connectionString = connectionString;
		}

		public bool Delete(Guid id)
		{
			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();

				using (var trans = con.BeginTransaction())
				{
					try
					{
						NpgsqlCommand cmd = criarComando(con);

						cmd.Transaction = trans;
						cmd.CommandText = @"DELETE FROM Acao WHERE Id=@Id";
						cmd.Parameters.AddWithValue("Id", id);
						cmd.ExecuteNonQuery();

						trans.Commit();
						return true;
					}
					catch (Exception ex)
					{
						trans.Rollback();
						throw ex;
					}
				}
			}
		}

		public Acao Find(Guid id)
		{
			Acao acao = null;
			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();

				NpgsqlCommand cmd = criarComando(con);

				cmd.CommandText = @"SELECT * FROM Acao WHERE Id=@Id";
				cmd.Parameters.AddWithValue("Id", id.ToString());

				var reader = cmd.ExecuteReader();

				while (reader.Read())
				{
					acao = new Acao();
					acao.Id = Guid.Parse(reader["Id"].ToString());
					acao.Descricao = reader["Descricao"].ToString();
					acao.Participantes = int.Parse(reader["Participantes"].ToString());
					acao.DataHora = DateTime.Parse(reader["DataHora"].ToString());					
				}
				reader.Close();

				cmd.CommandText = @"select c.'Id', c.'Codigo', c.'Descricao' from Acao as p join TipoAcao as c on p.'Tipo' = c.'Codigo' where p.'Id'=@Id";
				cmd.Parameters.Clear();
				cmd.Parameters.AddWithValue("Id", id);

				reader = cmd.ExecuteReader();

				while (reader.Read())
				{
					acao.TipoAcao = new TipoAcao()
					{
						Id = Guid.Parse(reader["Id"].ToString()),
						Codigo = int.Parse(reader["Codigo"].ToString()),
						Descricao = reader["Descricao"].ToString()
					};
				}
				reader.Close();

				cmd.CommandText = @"select c.'Id', c.'Cep', c.'Rua' , c.'Numero', c.'Complemento', c.'Bairro', c.'Cidade' from Acao as p join Endereco as c on p.'Endereco' = c.'Id' where p.'Id'=@Id";
				cmd.Parameters.Clear();
				cmd.Parameters.AddWithValue("Id", id);

				reader = cmd.ExecuteReader();

				while (reader.Read())
				{
					acao.Endereco = new Endereco()
					{
						Id = Guid.Parse(reader["Id"].ToString()),
						Cep = reader["Cep"].ToString(),
						Rua = reader["Rua"].ToString(),
						Numero = reader["Numero"].ToString(),
						Complemento = reader["Complemento"].ToString(),
						Bairro = reader["Bairro"].ToString(),
						Cidade = reader["Cidade"].ToString()
					};
				}

				reader.Close();
			}

			return acao;			
		}

		public List<Acao> FindAll()
		{
			List<Acao> acoes = new List<Acao>();

			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();

				NpgsqlCommand cmd = criarComando(con);

				cmd.CommandText = @"SELECT * FROM Acao";
				var reader = cmd.ExecuteReader();

				while (reader.Read())
				{
					Acao acao = new Acao();
					acao.Id = Guid.Parse(reader["Id"].ToString());
					acao.Descricao = reader["Descricao"].ToString();
					acao.Participantes = int.Parse(reader["Participantes"].ToString());
					acao.DataHora = DateTime.Parse(reader["DataHora"].ToString());

					acoes.Add(acao);
				}

				reader.Close();

				foreach (Acao action in acoes)
				{
					NpgsqlCommand cmdTipo = criarComando(con);

					cmdTipo.CommandText = @"select c.'Id', c.'Codigo', c.'Descricao' from Acao as p join TipoAcao as c on p.'Tipo' = c.'Codigo' where p.'Id'=@Id";
					cmdTipo.Parameters.AddWithValue("Id", action.TipoAcao.Id);
					var readerTipo = cmdTipo.ExecuteReader();

					while (readerTipo.Read())
					{
						action.TipoAcao = new TipoAcao()
						{
							Id = Guid.Parse(readerTipo["Id"].ToString()),
							Codigo = int.Parse(readerTipo["Codigo"].ToString()),
							Descricao = readerTipo["Descricao"].ToString()
						};
					}
					readerTipo.Close();

					NpgsqlCommand cmdEnd = criarComando(con);

					cmdEnd.CommandText = @"select c.'Id', c.'Cep', c.'Rua' , c.'Numero', c.'Complemento', c.'Bairro', c.'Cidade' from Acao as p join Endereco as c on p.'Endereco' = c.'Id' where p.'Id'=@Id";
					cmdEnd.Parameters.Clear();
					cmdEnd.Parameters.AddWithValue("Id", action.Id);

					var readerEnd = cmdEnd.ExecuteReader();

					while (reader.Read())
					{
						action.Endereco = new Endereco()
						{
							Id = Guid.Parse(readerEnd["Id"].ToString()),
							Cep = readerEnd["Cep"].ToString(),
							Rua = readerEnd["Rua"].ToString(),
							Numero = readerEnd["Numero"].ToString(),
							Complemento = readerEnd["Complemento"].ToString(),
							Bairro = readerEnd["Bairro"].ToString(),
							Cidade = readerEnd["Cidade"].ToString()
						};
					}

					reader.Close();
				}
			}

			return acoes;
		}

		public Guid Insert(Acao acao)
		{
			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();
				//Inicia a transação
				using (var trans = con.BeginTransaction())
				{
					try
					{
						NpgsqlCommand cmd = criarComando(con);
						cmd.Transaction = trans;
						cmd.CommandText = @"INSERT Into Acao (Id,Descricao,Participantes,DataHora,TipoAcao,Endereco) values (@Id,@Descricao,@Participantes,@DataHora,@TipoAcao,@Endereco)";
						cmd.Parameters.AddWithValue("Id", acao.Id);
						cmd.Parameters.AddWithValue("Descricao", acao.Descricao);
						cmd.Parameters.AddWithValue("Participantes", acao.Participantes);
						cmd.Parameters.AddWithValue("DataHora", acao.DataHora);
						cmd.Parameters.AddWithValue("TipoAcao", acao.TipoAcao.Id);
						cmd.Parameters.AddWithValue("Endereco", acao.Endereco.Id);
						cmd.ExecuteNonQuery();

						trans.Commit();
						return acao.Id;
					}
					catch (Exception ex)
					{
						trans.Rollback();
						throw ex;
					}
				}
			}
		}

		public Guid Update(Acao acao)
		{
			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();
				//Inicia a transação
				using (var trans = con.BeginTransaction())
				{
					try
					{
						NpgsqlCommand cmd = criarComando(con);
						cmd.Transaction = trans;
						cmd.CommandText = @"UPDATE SET Acao Id=@Id,Descricao=@Descricao,Participantes=@Participantes,DataHora=@DataHora,TipoAcao=@TipoAcao,Endereco=@Endereco";
						cmd.Parameters.AddWithValue("Id", acao.Id);
						cmd.Parameters.AddWithValue("Descricao", acao.Descricao);
						cmd.Parameters.AddWithValue("Participantes", acao.Participantes);
						cmd.Parameters.AddWithValue("DataHora", acao.DataHora);
						cmd.Parameters.AddWithValue("TipoAcao", acao.TipoAcao.Id);
						cmd.Parameters.AddWithValue("Endereco", acao.Endereco.Id);
						cmd.ExecuteNonQuery();

						trans.Commit();
						return acao.Id;
					}
					catch (Exception ex)
					{
						trans.Rollback();
						throw ex;
					}
				}
			}
		}

		// MÉTODOS PRIVADOS DO REPOSITORY
		private NpgsqlCommand criarComando(NpgsqlConnection con)
		{
			NpgsqlCommand cmd = new NpgsqlCommand();

			cmd.Connection = con;

			return cmd;
		}
	}
}
