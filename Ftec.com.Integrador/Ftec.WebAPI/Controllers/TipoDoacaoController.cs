﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Ftec.WebAPI.Application;
using Ftec.WebAPI.Application.DTO;
using Ftec.WebAPI.Domain.Interfaces;
using Ftec.WebAPI.Infra.Repository;
using Ftec.WebAPI.Models;

namespace Ftec.WebAPI.Controllers
{
	public class TipoDoacaoController : ApiController
	{
		private ITipoDoacaoRepository tipoDoacaoRepository;
		private TipoDoacaoApplication tipoDoacaoApplication;

		public TipoDoacaoController()
		{
			string connectionString = string.Empty;

			tipoDoacaoRepository = new Ftec.WebAPI.Infra.PsqlRepository.TipoDoacaoRepository(connectionString);
			tipoDoacaoApplication = new TipoDoacaoApplication(tipoDoacaoRepository);
		}

		public HttpResponseMessage Get()
		{
			try
			{
				List<TipoDoacao> tpDoacoes = ProcurarTodos();
				return Request.CreateResponse(HttpStatusCode.OK, tpDoacoes);
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		public HttpResponseMessage Get(Guid id)
		{
			try
			{
				TipoDoacao tpDoacao = Procurar(id);

				if (tpDoacao == null)
				{
					return Request.CreateResponse(HttpStatusCode.NotFound, "Registro não encontrado!");
				}
				else
				{
					return Request.CreateResponse(HttpStatusCode.OK, tpDoacao);
				}
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		public HttpResponseMessage Post([FromBody]TipoDoacao tpDoacao)
		{
			try
			{
				Guid id = Inserir(tpDoacao);
				return Request.CreateResponse(HttpStatusCode.OK, id);
			}
			catch (ApplicationException ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		public HttpResponseMessage Put(Guid id, [FromBody]TipoDoacao tpDoacao)
		{
			try
			{
				TipoDoacao tpDoc = Procurar(id);
				if (tpDoc == null)
				{
					return Request.CreateResponse(HttpStatusCode.NotFound, "Registro nao encontrado!");
				}
				else
				{
					Alterar(tpDoacao);
					return Request.CreateResponse(HttpStatusCode.OK, id);
				}
			}
			catch (ApplicationException ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		public HttpResponseMessage Delete(Guid id)
		{
			try
			{
				//Neste local faria a exclusão do cliente no repositorio
				TipoDoacao tpDoacao = Procurar(id);
				if (tpDoacao == null)
				{
					return Request.CreateResponse(HttpStatusCode.NotFound, "TIpo não encontrado");
				}
				else
				{
					Excluir(id);
					return Request.CreateResponse(HttpStatusCode.OK, id);
				}
			}
			catch (ApplicationException ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}


		// MÉTODOS PRIVADOS

		private List<TipoDoacao> ProcurarTodos()
		{
			var tpDoacoes = new List<TipoDoacao>();
			var tpDoacoesDTO = tipoDoacaoApplication.GetAll();

			foreach (TipoDoacaoDTO tpDoacaoDTO in tpDoacoesDTO)
			{
				tpDoacoes.Add(new TipoDoacao()
				{
					Id = tpDoacaoDTO.Id,
					Codigo = tpDoacaoDTO.Codigo,
					Descricao = tpDoacaoDTO.Descricao
				});
			}

			return tpDoacoes;
		}

		private TipoDoacao Procurar(Guid id)
		{
			var tipoDoacaoDTO = tipoDoacaoApplication.Get(id);

			return new TipoDoacao()
			{
				Id = tipoDoacaoDTO.Id,
				Codigo = tipoDoacaoDTO.Codigo,
				Descricao = tipoDoacaoDTO.Descricao
			};
		}

		private Guid Inserir(TipoDoacao tpDoacao)
		{
			TipoDoacaoDTO tpDoacaoDTO = new TipoDoacaoDTO()
			{
				Id = tpDoacao.Id,
				Codigo = tpDoacao.Codigo,
				Descricao = tpDoacao.Descricao
			};

			return tipoDoacaoApplication.Insert(tpDoacaoDTO);
		}

		private void Alterar(TipoDoacao tpDoacao)
		{
			TipoDoacaoDTO tpDoacaoDTO = new TipoDoacaoDTO()
			{
				Id = tpDoacao.Id,
				Codigo = tpDoacao.Codigo,
				Descricao = tpDoacao.Descricao
			};

			tipoDoacaoApplication.Update(tpDoacaoDTO);
		}

		private void Excluir(Guid id)
		{
			tipoDoacaoApplication.Delete(id);
		}
	}
}