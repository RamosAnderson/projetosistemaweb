﻿using Ftec.WebAPI.Domain.Entities;
using Ftec.WebAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Infra.Repository
{
	public class DoacaoRepository : IDoacaoRepository
	{
		private string connectionString;

		public DoacaoRepository(string connectionString)
		{
			this.connectionString = connectionString;
		}

		public bool Delete(Guid id)
		{
			throw new NotImplementedException();
		}

		public Doacao Find(Guid id)
		{
			throw new NotImplementedException();
		}

		public List<Doacao> FindAll()
		{
			throw new NotImplementedException();
		}

		public Guid Insert(Doacao doacao)
		{
			throw new NotImplementedException();
		}

		public Guid Update(Doacao doacao)
		{
			throw new NotImplementedException();
		}
	}
}
