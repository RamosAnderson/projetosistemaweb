﻿using Ftec.WebAPI.Application.Adapter;
using Ftec.WebAPI.Application.DTO;
using Ftec.WebAPI.Domain.Entities;
using Ftec.WebAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Application
{
	public class DoacaoApplication
	{
		IDoacaoRepository doacaoRepository;

		public DoacaoApplication(IDoacaoRepository doacaoRepository)
		{
			this.doacaoRepository = doacaoRepository;
		}

		public Guid Insert(DoacaoDTO doacaoDTO)
		{
			var voluntario = DoacaoAdapter.ToDomain(doacaoDTO);
			return doacaoRepository.Insert(voluntario);
		}

		public Guid Update(DoacaoDTO doacaoDTO)
		{
			var voluntario = DoacaoAdapter.ToDomain(doacaoDTO);
			return doacaoRepository.Update(voluntario);
		}

		public bool Delete(Guid id)
		{
			return doacaoRepository.Delete(id);
		}

		public DoacaoDTO Get(Guid id)
		{
			var voluntario = doacaoRepository.Find(id);
			return DoacaoAdapter.ToDTO(voluntario);
		}

		public List<DoacaoDTO> GetAll()
		{
			List<DoacaoDTO> doacoesDTO = new List<DoacaoDTO>();
			var doacoes = doacaoRepository.FindAll();

			foreach (Doacao doc in doacoes)
			{
				doacoesDTO.Add(DoacaoAdapter.ToDTO(doc));
			}

			return doacoesDTO;
		}
	}
}
