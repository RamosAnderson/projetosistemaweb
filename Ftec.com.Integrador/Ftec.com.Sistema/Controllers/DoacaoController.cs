﻿using Ftec.com.Sistema.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Windows.Forms;

namespace Ftec.com.Sistema.Controllers
{
    public class DoacaoController : Controller
    {
        // GET: Doacao
        public ActionResult Index()
        {
			/*if (Session["VoluntarioLogado"] != null)
			{
				// Libera somente para criar uma doação
			} else
			{
				if (Session["InstituicaoLogado"] != null)
				{*/
					// Libera para visualizar as doações, e resgatar
					List<Doacao> doacoes  = getListaDoacoes();					
				//}
			//}

            return View(doacoes);
        }

		public ActionResult Novo()
		{
			return View();
		}

		public ActionResult Resgatar(Guid Id)
		{

			var doacoes = getListaDoacoes();
			var doacao = doacoes.FirstOrDefault(l => l.Id == Id);

			ViewBag.Doacao = doacao;

			return View();
			
		}


		public ActionResult ProcessarGravacaoPost(Doacao doacao)
		{
			//MessageBox.Show("to no salvar");

			/*if (ModelState.IsValid)
			{*/
				//MessageBox.Show("ta valido");
				var doacoes = getListaDoacoes();
				doacoes.Add(doacao);

				Session["doacoes"] = doacoes;

				return RedirectToAction("Index");
			/*}
			else
			{
				MessageBox.Show("cai no else");
				return View("Novo", doacao);
			}*/
		}

		public ActionResult ProcessarResgateDoacaoPost(Doacao doacao)
		{
			
			var doacoes = getListaDoacoes();
			var auxDoacao = doacoes.FirstOrDefault(l => l.Id == doacao.Id);

			// verificar se deve gravar em banco que  a instituição resgatou a doação
		    doacoes.Remove(auxDoacao);


			Session["doacoes"] = doacoes;
			

			return RedirectToAction("Index");
		}

		private List<Doacao> getListaDoacoes()
		{
			// Doações da instituicao
			List<Doacao> doacoes;
			if (Session["doacoes"] == null)
			{
				doacoes = new List<Doacao>();
			}
			else
			{
				doacoes = (List<Doacao>)Session["doacoes"];
			}

			return doacoes;
		}

		


	}
}