﻿using Ftec.WebAPI.Domain.Entities;
using Ftec.WebAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace Ftec.WebAPI.Infra.PsqlRepository
{
	public class UsuarioRepository : IUsuarioRepository
	{
		private string connectionString;

		public UsuarioRepository(string connectionString)
		{
			this.connectionString = connectionString;
		}

		public bool Delete(Guid id)
		{
			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();

				using (var trans = con.BeginTransaction())
				{
					try
					{
						NpgsqlCommand cmd = new NpgsqlCommand();

						cmd.Connection = con;
						cmd.Transaction = trans;
						cmd.CommandText = @"DELETE FROM Usuario WHERE Id@Id";
						cmd.Parameters.AddWithValue("Id", id);
						cmd.ExecuteNonQuery();

						trans.Commit();
						return true;
					}
					catch (Exception ex)
					{
						trans.Rollback();
						throw ex;
					}
				}
			}				
		}

		public Usuario Find(Guid id)
		{
			Usuario usuario = null;

			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();

				NpgsqlCommand cmd = new NpgsqlCommand();

				cmd.Connection = con;
				cmd.CommandText = @"SELECT * FROM Usuario WHERE Id=@Id";
				cmd.Parameters.AddWithValue("Id", id.ToString());

				var reader = cmd.ExecuteReader();

				while (reader.Read())
				{
					usuario = new Usuario();
					usuario.Id = Guid.Parse(reader["Id"].ToString());
					usuario.Nome = reader["Nome"].ToString();
					usuario.Email = reader["Email"].ToString();
					usuario.Senha = reader["Senha"].ToString();
				}

				reader.Close();
			}

			return usuario;
		}

		public List<Usuario> FindAll()
		{
			List<Usuario> usuarios = new List<Usuario>();

			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();

				NpgsqlCommand cmd = new NpgsqlCommand();

				cmd.Connection = con;
				cmd.CommandText = @"SELECT * FROM Usuario";
				

				var reader = cmd.ExecuteReader();

				while (reader.Read())
				{
					Usuario usuario = new Usuario();
					usuario.Id = Guid.Parse(reader["Id"].ToString());
					usuario.Nome = reader["Nome"].ToString();
					usuario.Email = reader["Email"].ToString();
					usuario.Senha = reader["Senha"].ToString();

					usuarios.Add(usuario);
				}

				reader.Close();
			}

			return usuarios;
		}

		public Guid Insert(Usuario usuario)
		{
			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();
				//Inicia a transação
				using (var trans = con.BeginTransaction())
				{
					try
					{
						NpgsqlCommand cmd = new NpgsqlCommand();
						cmd.Connection = con;
						cmd.Transaction = trans;
						cmd.CommandText = @"INSERT INTO Usuario (Id, Nome, Email, Senha) values(@Id, @Nome, @Email, @Senha)";
						cmd.Parameters.AddWithValue("Id",usuario.Id);
						cmd.Parameters.AddWithValue("Nome",usuario.Nome);
						cmd.Parameters.AddWithValue("Email",usuario.Email);
						cmd.Parameters.AddWithValue("Senha",usuario.Senha);
						cmd.ExecuteNonQuery();

						trans.Commit();
						return usuario.Id;
					}
					catch (Exception ex)
					{
						trans.Rollback();
						throw ex;
					}
				}
			}
		}

		public Guid Update(Usuario usuario)
		{
			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();
				//Inicia a transação
				using (var trans = con.BeginTransaction())
				{
					try
					{
						NpgsqlCommand cmd = new NpgsqlCommand();
						cmd.Connection = con;
						cmd.Transaction = trans;
						cmd.CommandText = @"UPDATE Usuario SET Id=@Id, Nome=@Nome, Email=@Email, Senha=@Senha WHERE Id=@Id";
						cmd.Parameters.AddWithValue("Id", usuario.Id);
						cmd.Parameters.AddWithValue("Nome", usuario.Nome);
						cmd.Parameters.AddWithValue("Email", usuario.Email);
						cmd.Parameters.AddWithValue("Senha", usuario.Senha);
						cmd.ExecuteNonQuery();

						trans.Commit();
						return usuario.Id;
					}
					catch (Exception ex)
					{
						trans.Rollback();
						throw ex;
					}
				}
			}
		}
	}
}
