﻿using Ftec.WebAPI.Application.DTO;
using Ftec.WebAPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Application.Adapter
{
	public static class TipoDoacaoAdapter
	{
		public static TipoDoacaoDTO ToDTO(TipoDoacao tpDoacao)
		{
			return new TipoDoacaoDTO()
			{
				Id        = tpDoacao.Id,
				Codigo    = tpDoacao.Codigo,
				Descricao = tpDoacao.Descricao
			};
		}

		public static TipoDoacao ToDomain(TipoDoacaoDTO tpDoacao)
		{
			return new TipoDoacao()
			{
				Id        = tpDoacao.Id,
				Codigo    = tpDoacao.Codigo,
				Descricao = tpDoacao.Descricao
			};
		}
	}
}
