﻿using Ftec.WebAPI.Application.Adapter;
using Ftec.WebAPI.Application.DTO;
using Ftec.WebAPI.Domain.Entities;
using Ftec.WebAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Application
{
	public class TipoAcaoApplication
	{
		ITipoAcaoRepository tipoAcaoRepository;

		public TipoAcaoApplication(ITipoAcaoRepository tipoAcaoRepository)
		{
			this.tipoAcaoRepository = tipoAcaoRepository;
		}

		public Guid Insert(TipoAcaoDTO tipoAcaoDTO)
		{
			var tipoAcao = TipoAcaoAdapter.ToDomain(tipoAcaoDTO);
			return tipoAcaoRepository.Insert(tipoAcao);
		}

		public Guid Update(TipoAcaoDTO tipoAcaoDTO)
		{
			var tipoAcao = TipoAcaoAdapter.ToDomain(tipoAcaoDTO);
			return tipoAcaoRepository.Update(tipoAcao);
		}

		public bool Delete(Guid id)
		{
			return tipoAcaoRepository.Delete(id);
		}

		public TipoAcaoDTO Get(Guid id)
		{
			var tipoAcao = tipoAcaoRepository.Find(id);
			return TipoAcaoAdapter.ToDTO(tipoAcao);
		}

		public List<TipoAcaoDTO> GetAll()
		{
			List<TipoAcaoDTO> tpAcoesDto = new List<TipoAcaoDTO>();
			var tpAcoes = tipoAcaoRepository.FindAll();

			foreach (TipoAcao tpAcao in tpAcoes)
			{
				tpAcoesDto.Add(TipoAcaoAdapter.ToDTO(tpAcao));
			}

			return tpAcoesDto;
		}
	}
}
