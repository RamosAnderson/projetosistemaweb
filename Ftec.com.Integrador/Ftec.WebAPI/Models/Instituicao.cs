﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ftec.WebAPI.Models
{
	public class Instituicao
	{
		public Guid Id { get; set; }
		public string Razao { get; set; }
		public string Email { get; set; }
		public string Senha { get; set; }
		public string Cnpj { get; set; }
		public Boolean Ativo { get; set; }
		public Endereco Endereco { get; set; }
		public List<Doacao> Doacoes { get; set; }
	}
}