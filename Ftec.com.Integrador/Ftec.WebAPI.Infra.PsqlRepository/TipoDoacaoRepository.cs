﻿using Ftec.WebAPI.Domain.Entities;
using Ftec.WebAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace Ftec.WebAPI.Infra.PsqlRepository
{
	public class TipoDoacaoRepository : ITipoDoacaoRepository
	{
		private string connectionString;
		public TipoDoacaoRepository(string connectionString)
		{
			this.connectionString = connectionString;
		}
		public bool Delete(Guid id)
		{
			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();

				using (var trans = con.BeginTransaction())
				{
					try
					{
						NpgsqlCommand cmd = criarComando(con);

						cmd.Transaction = trans;
						cmd.CommandText = @"DELETE FROM TipoDoacao WHERE Id=@Id";
						cmd.Parameters.AddWithValue("Id", id);
						cmd.ExecuteNonQuery();

						trans.Commit();
						return true;
					}
					catch (Exception ex)
					{
						trans.Rollback();
						throw ex;
					}
				}
			}
		}

		public TipoDoacao Find(Guid id)
		{
			TipoDoacao tpDoacao = null;

			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();

				NpgsqlCommand cmd = criarComando(con);

				cmd.CommandText = @"SELECT * FROM TipoDoacao WHERE Id=@Id";
				cmd.Parameters.AddWithValue("Id", id.ToString());

				var reader = cmd.ExecuteReader();

				while (reader.Read())
				{
					tpDoacao = new TipoDoacao();
					tpDoacao.Id = Guid.Parse(reader["Id"].ToString());
					tpDoacao.Codigo = int.Parse(reader["Codigo"].ToString());
					tpDoacao.Descricao = reader["Descricao"].ToString();

				}

				reader.Close();
			}

			return tpDoacao;			
		}

		public List<TipoDoacao> FindAll()
		{
			List<TipoDoacao> tpDoacoes = new List<TipoDoacao>();

			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();

				NpgsqlCommand cmd = criarComando(con);
				cmd.CommandText = @"SELECT * FROM TipoDoacao";

				var reader = cmd.ExecuteReader();

				while (reader.Read())
				{
					TipoDoacao tpDoacao = new TipoDoacao();
					tpDoacao.Id = Guid.Parse(reader["Id"].ToString());
					tpDoacao.Codigo = int.Parse(reader["Codigo"].ToString());
					tpDoacao.Descricao = reader["Descricao"].ToString();

					tpDoacoes.Add(tpDoacao);
				}

				reader.Close();
			}
			return tpDoacoes;
		}

		public Guid Insert(TipoDoacao tpDoacao)
		{
			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();
				//Inicia a transação
				using (var trans = con.BeginTransaction())
				{
					try
					{
						NpgsqlCommand cmd = criarComando(con);
						cmd.Transaction = trans;
						cmd.CommandText = @"INSERT INTO TipoDoacao (Id,Codigo,Descricao) values (@Id,@Codigo,@Descricao)";
						cmd.Parameters.AddWithValue("Id", tpDoacao.Id);
						cmd.Parameters.AddWithValue("Codigo", tpDoacao.Codigo);
						cmd.Parameters.AddWithValue("Rua", tpDoacao.Descricao);
						cmd.ExecuteNonQuery();

						trans.Commit();
						return tpDoacao.Id;

					}
					catch (Exception ex)
					{
						trans.Rollback();
						throw ex;
					}
				}
			}
		}

		public Guid Update(TipoDoacao tpDoacao)
		{
			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();
				//Inicia a transação
				using (var trans = con.BeginTransaction())
				{
					try
					{
						NpgsqlCommand cmd = criarComando(con);
						cmd.Transaction = trans;
						cmd.CommandText = @"UPDATE TipoDoacao SET Id=@Id,Codigo=@Codigo,Descricao=@Descricao";
						cmd.Parameters.AddWithValue("Id", tpDoacao.Id);
						cmd.Parameters.AddWithValue("Codigo", tpDoacao.Codigo);
						cmd.Parameters.AddWithValue("Rua", tpDoacao.Descricao);
						cmd.ExecuteNonQuery();

						trans.Commit();
						return tpDoacao.Id;

					}
					catch (Exception ex)
					{
						trans.Rollback();
						throw ex;
					}
				}
			}
		}

		// MÉTODOS PRIVADOS DO REPOSITORY
		private NpgsqlCommand criarComando(NpgsqlConnection con)
		{
			NpgsqlCommand cmd = new NpgsqlCommand();

			cmd.Connection = con;

			return cmd;
		}
	}
}
