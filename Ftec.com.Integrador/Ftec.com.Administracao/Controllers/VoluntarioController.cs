﻿using Ftec.com.Administracao.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ftec.com.Administracao.Controllers
{
    public class VoluntarioController : Controller
    {
        // GET: Voluntario
        public ActionResult Index()
        {		

			var voluntarios = GetListaVoluntarios();
			if (!voluntarios.Any())
			{				
				criarVoluntariosPendentes(voluntarios);
				Session["voluntarios"] = voluntarios;
			}
			
			return View(voluntarios);
        }

		public ActionResult Visualizar(Guid Id)
		{
			var voluntarios = GetListaVoluntarios();
			var voluntario = voluntarios.FirstOrDefault(l => l.Id == Id);

			ViewBag.Voluntario = voluntario;

			return View();
		}	

		public ActionResult Ativar(Guid Id)
		{
			var voluntarios = GetListaVoluntarios();
			var voluntario = voluntarios.FirstOrDefault(l => l.Id == Id);

			voluntario.Ativo = true;
			voluntarios.Remove(voluntario);

			return RedirectToAction("Index");
		}

		private List<Voluntario> GetListaVoluntarios()
		{
			List<Voluntario> voluntarios;
			if(Session["voluntarios"] == null)
			{
				voluntarios = new List<Voluntario>();
			}
			else
			{
				voluntarios = (List<Voluntario>)Session["voluntarios"];
			}

			return voluntarios;
		}

		private void criarVoluntariosPendentes(List<Voluntario> voluntarios)
		{
			Voluntario voluntario1 = new Voluntario();
			voluntario1.Ativo      = false;
			voluntario1.Email      = "anderson@gmail.com";
			voluntario1.Nome       = "Anderson";

			voluntarios.Add(voluntario1);

			Voluntario voluntario2 = new Voluntario();
			voluntario2.Ativo      = false;
			voluntario2.Email      = "luis@gmail.com";
			voluntario2.Nome       = "Luis";

			voluntarios.Add(voluntario2);

			Voluntario voluntario3 = new Voluntario();
			voluntario3.Ativo = false;
			voluntario3.Email = "Alan@gmail.com";
			voluntario3.Nome = "Alan";

			voluntarios.Add(voluntario3);

			Voluntario voluntario4 = new Voluntario();
			voluntario4.Ativo = false;
			voluntario4.Email = "leonardo@gmail.com";
			voluntario4.Nome = "Leonardo";

			voluntarios.Add(voluntario4);
		}
	}
}