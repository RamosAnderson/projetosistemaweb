﻿using Ftec.WebAPI.Domain.Entities;
using Ftec.WebAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace Ftec.WebAPI.Infra.PsqlRepository
{
	public class DoacaoRepository : IDoacaoRepository
	{
		private string connectionString;
		public DoacaoRepository(string connectionString)
		{
			this.connectionString = connectionString;
		}

		public bool Delete(Guid id)
		{
			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();

				using (var trans = con.BeginTransaction())
				{
					try
					{
						NpgsqlCommand cmd = criarComando(con);

						cmd.Transaction = trans;
						cmd.CommandText = @"DELETE FROM Doacao WHERE Id=@Id";
						cmd.Parameters.AddWithValue("Id", id);
						cmd.ExecuteNonQuery();

						trans.Commit();
						return true;
					}
					catch (Exception ex)
					{
						trans.Rollback();
						throw ex;
					}
				}
			}
		}

		public Doacao Find(Guid id)
		{
			Doacao doacao = null;
			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();

				NpgsqlCommand cmd = criarComando(con);

				cmd.CommandText = @"SELECT * FROM Doacao WHERE Id=@Id";
				cmd.Parameters.AddWithValue("Id", id.ToString());

				var reader = cmd.ExecuteReader();

				while (reader.Read())
				{
					doacao            = new Doacao();
					doacao.Id         = Guid.Parse(reader["Id"].ToString());
					doacao.Descricao  = reader["Descricao"].ToString();
					doacao.Disponivel = bool.Parse(reader["Disponivel"].ToString());
					doacao.Valor      = double.Parse(reader["Valor"].ToString());
				}
				reader.Close();

				cmd.CommandText = @"select c.'Id', c.'Codigo', c.'Descricao' from Doacao as p join TipoDoacao as c on p.'Tipo' = c.'Codigo' where p.'Id'=@Id";
				cmd.Parameters.Clear();
				cmd.Parameters.AddWithValue("Id", id);
				
				reader = cmd.ExecuteReader();

				while (reader.Read())
				{
					doacao.Tipo = new TipoDoacao()
					{
						Id = Guid.Parse(reader["Id"].ToString()),
						Codigo = int.Parse(reader["Codigo"].ToString()),
						Descricao = reader["Descricao"].ToString()
					};
				}

				reader.Close();
			}

			return doacao;
		}

		public List<Doacao> FindAll()
		{
			List<Doacao> doacoes = new List<Doacao>();

			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();

				NpgsqlCommand cmd = criarComando(con);

				cmd.CommandText = @"SELECT * FROM Doacao";
				var reader = cmd.ExecuteReader();

				while (reader.Read())
				{
					Doacao doacao = new Doacao();
					doacao.Id = Guid.Parse(reader["Id"].ToString());
					doacao.Descricao = reader["Descricao"].ToString();
					doacao.Disponivel = bool.Parse(reader["Disponivel"].ToString());
					doacao.Valor = double.Parse(reader["Valor"].ToString());

					doacoes.Add(doacao);
				}

				reader.Close();

				foreach(Doacao doa in doacoes)
				{
					NpgsqlCommand cmdTipo = criarComando(con);

					cmdTipo.CommandText = @"select c.'Id', c.'Codigo', c.'Descricao' from Doacao as p join TipoDoacao as c on p.'Tipo' = c.'Codigo' where p.'Id'=@Id";
					cmdTipo.Parameters.AddWithValue("Id",doa.Tipo.Id);
					var readerTipo = cmdTipo.ExecuteReader();

					while (readerTipo.Read())
					{
						doa.Tipo = new TipoDoacao()
						{
							Id = Guid.Parse(readerTipo["Id"].ToString()),
							Codigo = int.Parse(readerTipo["Codigo"].ToString()),
							Descricao = readerTipo["Descricao"].ToString()
						};
					}
				}
			}

			return doacoes;
		}

		public Guid Insert(Doacao doacao)
		{
			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();
				//Inicia a transação
				using (var trans = con.BeginTransaction())
				{
					try
					{
						NpgsqlCommand cmd = criarComando(con);
						cmd.Transaction = trans;
						cmd.CommandText = @"INSERT Into Doacao (Id,Descricao,Disponivel,Valor,Tipo) values (@Id,@Descricao,@Disponivel,@Valor,@Tipo)";
						cmd.Parameters.AddWithValue("Id", doacao.Id);
						cmd.Parameters.AddWithValue("Descricao", doacao.Descricao);
						cmd.Parameters.AddWithValue("Disponivel", doacao.Disponivel);
						cmd.Parameters.AddWithValue("Valor", doacao.Valor);
						cmd.Parameters.AddWithValue("Tipo", doacao.Tipo.Codigo);
						cmd.ExecuteNonQuery();

						trans.Commit();
						return doacao.Id;
					}
					catch (Exception ex)
					{
						trans.Rollback();
						throw ex;
					}
				}
			}
		}

		public Guid Update(Doacao doacao)
		{
			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();
				//Inicia a transação
				using (var trans = con.BeginTransaction())
				{
					try
					{
						NpgsqlCommand cmd = criarComando(con);
						cmd.Transaction = trans;
						cmd.CommandText = @"UPDATE Doacao SET Id=@Id, Descricao=@Descricao, Disponivel=@Disponivel, Valor=@Valor, Tipo=@Tipo";
						cmd.Parameters.AddWithValue("Id", doacao.Id);
						cmd.Parameters.AddWithValue("Descricao", doacao.Descricao);
						cmd.Parameters.AddWithValue("Disponivel", doacao.Disponivel);
						cmd.Parameters.AddWithValue("Valor", doacao.Valor);
						cmd.Parameters.AddWithValue("Tipo", doacao.Tipo.Codigo);
						cmd.ExecuteNonQuery();

						trans.Commit();
						return doacao.Id;
					}
					catch (Exception ex)
					{
						trans.Rollback();
						throw ex;
					}
				}
			}
		}

		// MÉTODOS PRIVADOS DO REPOSITORY
		private NpgsqlCommand criarComando(NpgsqlConnection con)
		{
			NpgsqlCommand cmd = new NpgsqlCommand();

			cmd.Connection = con;

			return cmd;
		}
	}
}
