﻿using Ftec.com.Sistema.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ftec.com.Sistema.Controllers
{
    public class AcaoController : Controller
    {
        // GET: Acao
        public ActionResult Index()
        {
            return View();
        }

		public ActionResult Novo()
		{
			return View();
		}

		public ActionResult Editar(Guid Id)
		{
			return View();
		}

		public ActionResult ProcessarGravacaoPost(Acao acao) {
			return View("Index");
		}

		public ActionResult ProcessarAlteracaoPost(Acao acao)
		{
			return View("Index");
		}

	}
}