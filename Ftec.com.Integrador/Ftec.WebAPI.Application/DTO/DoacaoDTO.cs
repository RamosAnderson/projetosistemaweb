﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Application.DTO
{
	public class DoacaoDTO
	{
		public Guid Id { get; set; }
		public string Descricao { get; set; }
		public TipoDoacaoDTO Tipo { get; set; }
		public Double Valor { get; set; }
		public bool Disponivel { get; set; }
	}
}
