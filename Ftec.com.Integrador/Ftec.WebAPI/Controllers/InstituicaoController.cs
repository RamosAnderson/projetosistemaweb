﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Ftec.WebAPI.Application;
using Ftec.WebAPI.Application.DTO;
using Ftec.WebAPI.Domain.Interfaces;
using Ftec.WebAPI.Infra.Repository;
using Ftec.WebAPI.Models;


namespace Ftec.WebAPI.Controllers
{
	public class InstituicaoController : ApiController
	{
		private IInstituicaoRepository instituicaoRepository;
		private InstituicaoApplication instituicaoApplication;

		public InstituicaoController()
		{
			string connectionString = string.Empty;

			instituicaoRepository = new Ftec.WebAPI.Infra.PsqlRepository.InstituicaoRepository(connectionString);
			instituicaoApplication = new InstituicaoApplication(instituicaoRepository);
		}

		public HttpResponseMessage Get()
		{
			try
			{
				List<Instituicao> instituicoes = ProcurarTodos();
				return Request.CreateResponse(HttpStatusCode.OK, instituicoes);

			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		public HttpResponseMessage Get(Guid id)
		{
			try
			{
				Instituicao inst = Procurar(id);
				if (inst == null)
				{
					return Request.CreateResponse(HttpStatusCode.NotFound, "Registro não encontrado!");
				}
				else
				{
					return Request.CreateResponse(HttpStatusCode.OK, inst);
				}
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		public HttpResponseMessage Post([FromBody]Instituicao instituicao)
		{
			try
			{
				Guid id = Inserir(instituicao);
				return Request.CreateResponse(HttpStatusCode.OK, id);
			}
			catch (ApplicationException ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		public HttpResponseMessage Put(Guid id, [FromBody]Instituicao instituicao)
		{
			try
			{
				Instituicao inst = Procurar(id);
				if (inst == null)
				{
					return Request.CreateResponse(HttpStatusCode.NotFound, "Instituicao a ser alterada, não encontrada");
				} else
				{
					Alterar(instituicao);
					return Request.CreateResponse(HttpStatusCode.OK, id);
				}
			}
			catch (ApplicationException ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		public HttpResponseMessage Delete(Guid id)
		{
			try
			{
				Instituicao inst = Procurar(id);
				if (inst == null)
				{
					return Request.CreateResponse(HttpStatusCode.NotFound, "Instituicao a ser excluida não encontrada!");
				}
				else
				{
					Excluir(id);
					return Request.CreateResponse(HttpStatusCode.OK, id);
				}
			}
			catch (ApplicationException ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
			}
			catch (Exception ex)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
		}

		/*MÉTODOS PRIVADOS DO CONTROLLER*/
		private List<Instituicao> ProcurarTodos()
		{
			var instituicoes    = new List<Instituicao>();
			var instituicoesDTO = instituicaoApplication.GetAll();

			foreach (InstituicaoDTO instituicaoDTO in instituicoesDTO)
			{
				Instituicao inst = new Instituicao()
				{
					Id = instituicaoDTO.Id,
					Ativo = instituicaoDTO.Ativo,
					Cnpj = instituicaoDTO.Cnpj,
					Email = instituicaoDTO.Email,
					Razao = instituicaoDTO.Razao,
					Senha = instituicaoDTO.Razao,
					Endereco = new Endereco()
					{
						Id = instituicaoDTO.Endereco.Id,
						Bairro = instituicaoDTO.Endereco.Bairro,
						Cep = instituicaoDTO.Endereco.Cep,
						Cidade = instituicaoDTO.Endereco.Cidade,
						Complemento = instituicaoDTO.Endereco.Complemento,
						Numero = instituicaoDTO.Endereco.Numero,
						Rua = instituicaoDTO.Endereco.Rua
					}
				};

				foreach (DoacaoDTO doacao in instituicaoDTO.Doacoes)
				{
					var docAux = new Doacao()
					{
						Id = doacao.Id,
						Descricao = doacao.Descricao,
						Disponivel = doacao.Disponivel,
						Valor = doacao.Valor,
						Tipo = new TipoDoacao()
						{
							Id = doacao.Tipo.Id,
							Codigo = doacao.Tipo.Codigo,
							Descricao = doacao.Tipo.Descricao
						}
					};

					inst.Doacoes.Add(docAux);
				}

				instituicoes.Add(inst);
			}

			return instituicoes;
		}

		private Instituicao Procurar(Guid id)
		{
			var instituicao = instituicaoApplication.Get(id);

			Instituicao inst = new Instituicao()
			{
				Id = instituicao.Id,
				Ativo = instituicao.Ativo,
				Cnpj = instituicao.Cnpj,
				Email = instituicao.Email,
				Razao = instituicao.Razao,
				Senha = instituicao.Razao,
				Endereco = new Endereco()
				{
					Id = instituicao.Endereco.Id,
					Bairro = instituicao.Endereco.Bairro,
					Cep = instituicao.Endereco.Cep,
					Cidade = instituicao.Endereco.Cidade,
					Complemento = instituicao.Endereco.Complemento,
					Numero = instituicao.Endereco.Numero,
					Rua = instituicao.Endereco.Rua
				}
			};

			foreach (DoacaoDTO doacao in instituicao.Doacoes)
			{
				var docAux = new Doacao()
				{
					Id = doacao.Id,
					Descricao = doacao.Descricao,
					Disponivel = doacao.Disponivel,
					Valor = doacao.Valor,
					Tipo = new TipoDoacao()
					{
						Id = doacao.Tipo.Id,
						Codigo = doacao.Tipo.Codigo,
						Descricao = doacao.Tipo.Descricao
					}
				};

				inst.Doacoes.Add(docAux);
			}

			return inst;
		}

		private Guid Inserir(Instituicao instituicao)
		{
			InstituicaoDTO instDTO = new InstituicaoDTO()
			{
				Id = instituicao.Id,
				Ativo = instituicao.Ativo,
				Cnpj = instituicao.Cnpj,
				Email = instituicao.Email,
				Razao = instituicao.Razao,
				Senha = instituicao.Razao,
				Endereco = new EnderecoDTO()
				{
					Id = instituicao.Endereco.Id,
					Bairro = instituicao.Endereco.Bairro,
					Cep = instituicao.Endereco.Cep,
					Cidade = instituicao.Endereco.Cidade,
					Complemento = instituicao.Endereco.Complemento,
					Numero = instituicao.Endereco.Numero,
					Rua = instituicao.Endereco.Rua
				}
			};

			foreach (Doacao doacao in instituicao.Doacoes)
			{
				var docAux = new DoacaoDTO()
				{
					Id = doacao.Id,
					Descricao = doacao.Descricao,
					Disponivel = doacao.Disponivel,
					Valor = doacao.Valor,
					Tipo = new TipoDoacaoDTO()
					{
						Id = doacao.Tipo.Id,
						Codigo = doacao.Tipo.Codigo,
						Descricao = doacao.Tipo.Descricao
					}
				};

				instDTO.Doacoes.Add(docAux);
			}

			return instituicaoApplication.Insert(instDTO);
		}

		private void Alterar(Instituicao instituicao)
		{
			InstituicaoDTO instDTO = new InstituicaoDTO()
			{
				Id = instituicao.Id,
				Ativo = instituicao.Ativo,
				Cnpj = instituicao.Cnpj,
				Email = instituicao.Email,
				Razao = instituicao.Razao,
				Senha = instituicao.Razao,
				Endereco = new EnderecoDTO()
				{
					Id = instituicao.Endereco.Id,
					Bairro = instituicao.Endereco.Bairro,
					Cep = instituicao.Endereco.Cep,
					Cidade = instituicao.Endereco.Cidade,
					Complemento = instituicao.Endereco.Complemento,
					Numero = instituicao.Endereco.Numero,
					Rua = instituicao.Endereco.Rua
				}
			};

			foreach (Doacao doacao in instituicao.Doacoes)
			{
				var docAux = new DoacaoDTO()
				{
					Id = doacao.Id,
					Descricao = doacao.Descricao,
					Disponivel = doacao.Disponivel,
					Valor = doacao.Valor,
					Tipo = new TipoDoacaoDTO()
					{
						Id = doacao.Tipo.Id,
						Codigo = doacao.Tipo.Codigo,
						Descricao = doacao.Tipo.Descricao
					}
				};

				instDTO.Doacoes.Add(docAux);
			}

			instituicaoApplication.Update(instDTO);
		}

		private void Excluir(Guid id)
		{
			instituicaoApplication.Delete(id);
		}
	}
}