﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Domain.Entities
{
	public class Doacao
	{
		public Guid Id { get; set; }
		public string Descricao { get; set; }
		public TipoDoacao Tipo { get; set; }
		public Double Valor { get; set; }
		public bool Disponivel { get; set; }
	}
}
