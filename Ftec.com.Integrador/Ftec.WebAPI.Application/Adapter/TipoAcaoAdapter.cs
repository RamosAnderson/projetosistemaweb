﻿using Ftec.WebAPI.Application.DTO;
using Ftec.WebAPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Application.Adapter
{
	public static class TipoAcaoAdapter
	{
		public static TipoAcaoDTO ToDTO(TipoAcao tpAcao)
		{
			return new TipoAcaoDTO()
			{
				Id = tpAcao.Id,
				Codigo = tpAcao.Codigo,
				Descricao = tpAcao.Descricao
			};
		}

		public static TipoAcao ToDomain(TipoAcaoDTO tpAcao)
		{
			return new TipoAcao()
			{
				Id = tpAcao.Id,
				Codigo = tpAcao.Codigo,
				Descricao = tpAcao.Descricao
			};
		}
	}
}
