﻿using Ftec.WebAPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ftec.WebAPI.Domain.Interfaces
{
	public interface IInstituicaoRepository
	{
		Guid Insert(Instituicao instituicao);
		Instituicao Find(Guid id);
		List<Instituicao> FindAll();
		Guid Update(Instituicao instituicao);
		bool Delete(Guid id);
	}
}
