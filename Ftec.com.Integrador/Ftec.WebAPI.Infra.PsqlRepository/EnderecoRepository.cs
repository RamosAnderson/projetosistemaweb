﻿using Ftec.WebAPI.Domain.Entities;
using Ftec.WebAPI.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace Ftec.WebAPI.Infra.PsqlRepository
{
	public class EnderecoRepository : IEnderecoRepository
	{
		private string connectionString;
		public EnderecoRepository(string connectionString)
		{
			this.connectionString = connectionString;
		}
		public bool Delete(Guid id)
		{
			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();

				using (var trans = con.BeginTransaction())
				{
					try
					{
						NpgsqlCommand cmd = criarComando(con);

						cmd.Transaction = trans;
						cmd.CommandText = @"DELETE FROM Endereco WHERE Id=@Id";
						cmd.Parameters.AddWithValue("Id", id);
						cmd.ExecuteNonQuery();

						trans.Commit();
						return true;
					}
					catch (Exception ex)
					{
						trans.Rollback();
						throw ex;
					}
				}
			}
		}

		public Endereco Find(Guid id)
		{
			Endereco endereco = null;

			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();

				NpgsqlCommand cmd = criarComando(con);

				cmd.CommandText = @"SELECT * FROM Endereco WHERE Id=@Id";
				cmd.Parameters.AddWithValue("Id", id.ToString());

				var reader = cmd.ExecuteReader();

				while (reader.Read())
				{
					endereco = new Endereco();
					endereco.Id = Guid.Parse(reader["Id"].ToString());
					endereco.Cidade = reader["Cidade"].ToString();
					endereco.Complemento = reader["Complemento"].ToString();
					endereco.Bairro = reader["Bairro"].ToString();
					endereco.Cep = reader["Cep"].ToString();
					endereco.Numero = reader["Numero"].ToString();
					endereco.Rua = reader["Rua"].ToString();
				}

				reader.Close();
			}

			return endereco;
		}

		public List<Endereco> FindAll()
		{
			List<Endereco> enderecos = new List<Endereco>();

			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();

				NpgsqlCommand cmd = criarComando(con);
				cmd.CommandText = @"SELECT * FROM Endereco";

				var reader = cmd.ExecuteReader();

				while (reader.Read())
				{
					Endereco endereco = new Endereco();
					endereco.Id = Guid.Parse(reader["Id"].ToString());
					endereco.Cidade = reader["Cidade"].ToString();
					endereco.Complemento = reader["Complemento"].ToString();
					endereco.Bairro = reader["Bairro"].ToString();
					endereco.Cep = reader["Cep"].ToString();
					endereco.Numero = reader["Numero"].ToString();
					endereco.Rua = reader["Rua"].ToString();

					enderecos.Add(endereco);
				}

				reader.Close();
			}
			return enderecos;
		}

		public Guid Insert(Endereco endereco)
		{
			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();
				//Inicia a transação
				using (var trans = con.BeginTransaction())
				{
					try
					{
						NpgsqlCommand cmd = criarComando(con);
						cmd.Transaction = trans;
						cmd.CommandText = @"INSERT INTO Endereco (Id,Cep,Rua,Numero,Complemento,Bairro,Cidade) values (@Id,@Cep,@Rua,@Numero,@Complemento,@Bairro,@Cidade)";
						cmd.Parameters.AddWithValue("Id", endereco.Id);
						cmd.Parameters.AddWithValue("Cep", endereco.Cep);
						cmd.Parameters.AddWithValue("Rua", endereco.Rua);
						cmd.Parameters.AddWithValue("Numero", endereco.Numero);
						cmd.Parameters.AddWithValue("Complemento", endereco.Complemento);
						cmd.Parameters.AddWithValue("Bairro", endereco.Bairro);
						cmd.Parameters.AddWithValue("Cidade", endereco.Cidade);
						cmd.ExecuteNonQuery();

						trans.Commit();
						return endereco.Id;

					}
					catch (Exception ex)
					{
						trans.Rollback();
						throw ex;
					}
				}
			}
		}

		public Guid Update(Endereco endereco)
		{
			using (NpgsqlConnection con = new NpgsqlConnection(connectionString))
			{
				con.Open();
				//Inicia a transação
				using (var trans = con.BeginTransaction())
				{
					try
					{
						NpgsqlCommand cmd = criarComando(con);
						cmd.Transaction = trans;
						cmd.CommandText = @"UPDATE Endereco SET Id=@Id,Cep=@Cep,Rua=@Rua,Numero=@Numero,Complemento=@Complemento,Bairro=@Bairro,Cidade=@Cidade";
						cmd.Parameters.AddWithValue("Id",		   endereco.Id);
						cmd.Parameters.AddWithValue("Cep",		   endereco.Cep);
						cmd.Parameters.AddWithValue("Rua",	       endereco.Rua);
						cmd.Parameters.AddWithValue("Numero",	   endereco.Numero);
						cmd.Parameters.AddWithValue("Complemento", endereco.Complemento);
						cmd.Parameters.AddWithValue("Bairro",      endereco.Bairro);
						cmd.Parameters.AddWithValue("Cidade",	   endereco.Cidade);
						cmd.ExecuteNonQuery();

						trans.Commit();
						return endereco.Id;

					}
					catch (Exception ex)
					{
						trans.Rollback();
						throw ex;
					}
				}
			}
		}

		// MÉTODOS PRIVADOS DO REPOSITORY
		private NpgsqlCommand criarComando(NpgsqlConnection con)
		{
			NpgsqlCommand cmd = new NpgsqlCommand();

			cmd.Connection = con;

			return cmd;
		}
	}
}
