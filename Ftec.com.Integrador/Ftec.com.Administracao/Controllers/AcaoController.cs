﻿using Ftec.com.Administracao.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ftec.com.Administracao.Controllers
{
    public class AcaoController : Controller
    {
        // GET: Acao
        public ActionResult Index()
        {
			var acoes = getListaAcoes();
			return View(acoes);
        }

		public ActionResult Novo()
		{
			return View();
		}

		public ActionResult Editar(Guid Id)
		{
			var acoes = getListaAcoes();
			var acao = acoes.FirstOrDefault(l => l.Id == Id);
			ViewBag.Acao = acao;

			return View();
		}

		public ActionResult Excluir(Guid Id)
		{
			var acoes = getListaAcoes();
			var acao = acoes.FirstOrDefault(l => l.Id == Id);
			acoes.Remove(acao);

			Session["acoes"] = acoes;

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult ProcessarGravacaoPost(Acao acao)
		{
			if (ModelState.IsValid)
			{
				var acoes = getListaAcoes();
				acoes.Add(acao);

				Session["acoes"] = acoes;

				return RedirectToAction("Index");
			}
			else
			{
				return View("Novo", acao);
			}
		}

		public ActionResult ProcessarAlteracaoPost(Acao acao)
		{
			var acoes = getListaAcoes();
			var acaoOld = acoes.FirstOrDefault(l => l.Id == acao.Id);

			acoes.Remove(acaoOld);
			acoes.Add(acao);

			Session["acoes"] = acoes;

			return RedirectToAction("Index");
		}

		public List<Acao> getListaAcoes()
		{
			List<Acao> acoes;
			if (Session["acoes"] == null)
			{
				acoes = new List<Acao>();
			}
			else
			{
				acoes = (List<Acao>)Session["acoes"];
			}

			return acoes;
		}
	}
}